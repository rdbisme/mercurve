#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" A script to plot the phase space for H and G

Usage:
    phase-space <delta>


"""

import matplotlib.pyplot as plt
import numpy as np

import docopt

from scipy.optimize import newton


def H2G(G):
    return (np.sqrt(G))


if __name__ == '__main__':

    args = docopt.docopt(__doc__)
    delta = float(args['<delta>'])

    def oblique_line(G):
        return (delta*G/2 + 1/2/delta)

    fig = plt.figure(frameon=False)

    # Find intersection
    G_inter = newton(
        lambda G: H2G(G) - oblique_line(G), 0)

    plt.plot(G_inter, H2G(G_inter), 'o', color='black')
    plt.plot(G_inter, -H2G(G_inter), 'o', color='black')

    # Slope at the intersection
    angle = np.arctan(delta/4)
    plt.text(G_inter, 1.4*H2G(G_inter),
             r'$H=\frac{\delta G}{2} + \frac{1}{2 \delta}$',
             rotation=np.rad2deg(angle)
             )

    plt.text(G_inter, -1.2*H2G(G_inter),
             r'$H=-\frac{\delta G}{2} - \frac{1}{2 \delta}$',
             rotation=-np.rad2deg(angle)
             )

    G_lim = (-2*G_inter, 2*G_inter)

    # Plot lines
    G = np.linspace(G_lim[0], G_lim[1], 100)
    plt.plot(G, oblique_line(G), '-', color='black')
    plt.plot(G, 1/delta*np.ones(G.shape), '-', color='gray')
    plt.plot(G, -1/delta*np.ones(G.shape), '-', color='gray')
    plt.text(G_lim[0] + 0.1*np.abs(G_lim[0]), 1.05*1/delta,
             r'$\frac{1}{\delta}$')
    plt.text(G_lim[0] + 0.1*np.abs(G_lim[0]), -1.15*1/delta,
             r'$-\frac{1}{\delta}$')

    # Only negative part
    line = -oblique_line(G)
    neg = np.where(line <= 0)
    line = line[neg]
    Gn = G[neg]
    plt.plot(Gn, line, '-', color='black')

    # Only positive part
    Gp = np.linspace(0, G_lim[1], 100)
    plt.plot(Gp, H2G(Gp), '-', color='black')
    plt.plot(Gp, -H2G(Gp), '-', color='black')

    plt.text(0.05, 0, r"$H^2=G$")

    # G > 0, H > 0
    G = np.linspace(0, G_lim[1], 100)
    plt.fill_between(G, oblique_line(G), H2G(G),
                     where=oblique_line(G) <= 1/delta,
                     color='gray', alpha=0.5)

    # G < 0, H > 0
    G = np.linspace(G_lim[0], 0, 100)
    line = oblique_line(G)
    plt.fill_between(G, line, color='gray', alpha=0.5)

    # G < 0, H < 0
    G = np.linspace(G_lim[0], 0, 100)
    line = -oblique_line(G)
    plt.fill_between(G, line, where=line <= 0, color='gray', alpha=0.5)

    # G > 0, H < 0
    G = np.linspace(0, G_lim[1], 100)
    line = -oblique_line(G)
    plt.fill_between(G, line, -H2G(G),
                     where=line >= -1/delta,
                     color='gray', alpha=0.5)

    plt.text(-1.1/delta/delta, -0.1, r'$-\frac{1}{\delta^2}$')

    plt.axis('off')
    plt.tight_layout()
    plt.savefig(f'phase-space-{delta}.eps', format='eps', dpi=300)
    plt.show()
