""" This module contains all the handy functions related to plotting. """
import numpy as np

import matplotlib.pyplot as plt

from .math import nonlinspace


def plot_histogram2d(x, y, bins, weights=None, nonlin=False, range=None):
    """ This function generates a 2D histogram and plots it as a contourf.

    Args:

        nonlinspace(bool): If true it use a non linear contour colormap that
        has more levels close to zero range

    """

    if weights is None:
        weights = np.ones(len(x))

    xy_bins, x_edges, y_edges = np.histogram2d(
        x, y, bins, weights=weights/np.sum(weights),
        range=range)

    x_centers = (x_edges[:-1] + x_edges[1:])/2
    y_centers = (y_edges[:-1] + y_edges[1:])/2

    if nonlin:
        levels = nonlinspace(np.min(xy_bins), np.max(xy_bins), 100)
    else:
        levels = 100

    plt.figure()
    cs = plt.contourf(x_centers, y_centers, xy_bins.T, levels=levels)
    plt.contour(cs, colors='#444444', linewidths=0.5, levels=cs.levels[::12])
    plt.colorbar(cs)
    plt.locator_params(axis='x', nbins=6)
    plt.xlabel('Relative Error')
    plt.ylabel('Cell Area')
    plt.tight_layout()


def plot_histogram(x, bins, weights=None, range=None):
    """ This function generates a 1D histogram plot """

    if weights is None:
        weights = np.ones(len(x))

    x_bins, x_edges = np.histogram(
        x, bins, weights=weights/np.sum(weights),
        range=range)

    x_centers = (x_edges[:-1] + x_edges[1:])/2

    plt.figure()
    plt.plot(x_centers, x_bins)
    plt.xlabel('Relative Error')
    plt.ylabel('Area-weighted point density')
    plt.tight_layout()

    return x_centers, x_bins
