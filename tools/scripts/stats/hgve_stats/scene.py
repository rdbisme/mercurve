# hg(v)e Stats
# Copyright © 2019 Ruben Di Battista
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of Ruben Di Battista.
#
# This module implements the `Scene` abstraction, that means a view of the data
# to be displayed and/or saved.

""" The various scenes that can be used in a :class:`Pipeline` to perform the
post-processing """
import abc
import re

import matplotlib.pyplot as plt
import numpy as np
import numpy.lib.recfunctions as np_lib_recfuncs

from typing import TYPE_CHECKING

from .plotting import plot_histogram, plot_histogram2d

if TYPE_CHECKING:
    from .pipeline import Pipeline


class Scene(abc.ABC):
    """ A class representing a scene to be rendered. It can be a self-standing
    scene that takes as input the data to visualize and then renders it or,
    instead, taking as input another scene and using its output data as input

    Attributes
    ---------
    pipeline
        The :class:`Pipeline` object this scene is member of. You can access
        global attributes
    """

    pipeline: 'Pipeline'
    output_data: np.ndarray

    @abc.abstractmethod
    def process(self):
        """ This is the method called by the Pipeline object. Feel free to
        define additional methods in the concrete Scene to implement specific
        steps of the Scene processing, for example: compute, render, save
        """
        return NotImplementedError

    @property
    def dep(self):
        return self._dep

    @dep.setter
    def dep(self, scene: 'Scene'):
        self._dep = scene
        if scene is not None:
            self.data = scene.output_data


class ReadDataScene(Scene):
    """ This `Scene` just reads the curvature data from a file
    """

    def _read(self):
        # We assume here that the output file has an header from which
        # we can inspect the column names
        with open(self.pipeline.filename, 'r') as f:
            first_line = f.readline()

        # We assume the column names are separated by tabs.
        # TODO: Think about a more robust way
        columns = re.split(r'\t+', first_line.rstrip('\t\n'))

        # Clean
        columns = [col.strip() for col in columns]
        dtype = np.dtype([(col, 'f8') for col in columns])

        # Read the actual data
        data = np.loadtxt(self.pipeline.filename, skiprows=1)

        self.output_data = data.view(dtype).reshape(-1)

    def process(self):
        self._read()


class SanitizeScene(Scene):
    """ This `Scene` just sanitize the input data removing NaNs
    """

    def _compute(self):
        # Transform in simple n-dimensional numpy array without field names
        r = self.data.shape[0]
        c = len(self.data.dtype.names)
        view_data = self.data.view('f8').reshape(r, c)

        # Filter out NaNs
        indices = np.where(~(np.isnan(view_data[:, 0])))
        for col in view_data[1:, :]:
            indices = np.intersect1d(indices, np.where(~(np.isnan(col))))

        self.output_data = view_data.view(self.data.dtype).reshape(-1)

    def process(self):
        self._compute()


class GaussBonnet(Scene):
    """ This scene stores the integral of the Gauss curvature of the surface
    in order to compute the Gauss Bonnet theorem for each time instant """

    def _compute(self):
        # Store Gauss-Bonnet in the global state
        # I need to use a temp dict instead of the mp proxy doesn't propagate
        # nested dicts
        G = self.data['Gauss Curvature']/self.pipeline.G_def
        H = self.data['Mean Curvature']/self.pipeline.H_def
        A = self.data['Area']/self.pipeline.A_def
        temp_storage = dict(self.pipeline.storage)
        temp_storage[self.pipeline.filename].update({
            "Gauss Bonnet": np.sum(np.multiply(G, A))/4/np.pi,
            "Gauss Curvature": np.sum(np.multiply(G, A))/np.sum(A),
            "Mean Curvature": np.sum(np.multiply(H, A))/np.sum(A),
            "Area": np.sum(A),
        })

        self.pipeline.storage.update(temp_storage)

        self.output_data = self.data

    def process(self):
        self._compute()


class QualityScene(Scene):
    """ This scenes generates plots for mesh quality"""

    def _compute(self):
        self.quality = self.data["Quality"]

        self.output_data = self.data

        r = len(self.output_data)
        self.nbins = int(np.round(r**0.4))

    def _render(self):
        # Area-weighted 1D PDF
        qual, bins = plot_histogram(self.quality, self.nbins, range=(40, 1000))
        plt.xlabel('Quality')
        plt.ylabel('Percentage of points')
        plt.savefig(f'{self.pipeline.basename}-quality.{self.pipeline.fmt}',
                    format=self.pipeline.fmt, dpi=300)
        plt.close()
        np.savetxt(f'{self.pipeline.basename}-quality.stat',
                   np.array([qual, bins]).T,
                   header='Quality_PDF',
                   comments='# ')

    def process(self):
        self._compute()
        self._render()


class ErrorMapsScene(Scene):
    """ This `Scene` generate area-weighted error plots w.r.t. a reference
    value. The function to compute the error must be available as a function of
    the `self.data` inside the `self.pipeline`.

    Attributes:
        error_fun(function): The function that operates on `self.data` to
            compute the error
    """

    def __init__(self, error_fun, *args, **kwargs):
        self.error_fun = error_fun

        super().__init__(*args, **kwargs)

    def _compute(self):
        # Useful attributes
        self.A = self.data['Area']
        self.H = self.data['Mean Curvature']
        self.G = self.data['Gauss Curvature']
        self.quality = self.data['Quality']
        self.error = self.error_fun(self.data)

        # Instead of using reference data we need to ship another function
        # that estimates L1 error. Maybe putting the same ErrorMapScene but
        # with a different error_fun
        H_L1_error = np.sum(
            np.multiply(self.A,
                        np.abs(self.H - self.pipeline.H_def) / np.sum(self.A)))
        G_L1_error = np.sum(
            np.multiply(self.A,
                        np.abs(self.G - self.pipeline.G_def) / np.sum(self.A)))

        # Store global error in the global state
        # I need to use a temp dict instead of the mp proxy doesn't propagate
        # nested dicts
        temp_storage = dict(self.pipeline.storage)
        temp_storage[self.pipeline.filename].update(
            {"H L1 Error": H_L1_error},
            {"G L1 Error": G_L1_error}
        )

        self.pipeline.storage.update(temp_storage)

        # Add error column to output dataset
        self.output_data = self.data
        self.output_data = np_lib_recfuncs.append_fields(
            self.output_data, "Error", self.error.reshape(-1))

        r = len(self.output_data)
        self.nbins = int(np.round(r**0.4))

    def _render(self):
        # Area-weighted 2D PDF
        plot_histogram2d(self.error, self.A, self.nbins, weights=self.A,
                         nonlin=True)
        plt.savefig(
            f'{self.pipeline.basename}-area-error-pdf.{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

        # Area-weighted 2D PDFs for quality instead of area
        plot_histogram2d(self.error, self.quality, self.nbins, weights=self.A,
                         nonlin=True,
                         range=(
                             (np.min(self.error), np.max(self.error)),
                             (40, 1000)
                         )
                         )
        plt.ylabel("Mesh Quality")
        plt.savefig(
            f'{self.pipeline.basename}-quality-area-error-pdf'
            f'.{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

        # Area-weighted 1D PDF
        err, bins = plot_histogram(self.error, self.nbins, weights=self.A)
        plt.savefig(f'{self.pipeline.basename}-errors.{self.pipeline.fmt}',
                    format=self.pipeline.fmt, dpi=300)
        plt.close()
        np.savetxt(f'{self.pipeline.basename}-errors.stat',
                   np.array([err, bins]).T,
                   header='Error Area_Weighted_PDF',
                   comments='# ')

        # Simple PDF w/o Area-weighting
        plot_histogram2d(self.error, self.A, self.nbins, nonlin=True)
        plt.savefig(f'{self.pipeline.basename}-error-pdf.{self.pipeline.fmt}',
                    format=self.pipeline.fmt,
                    dpi=300)

        plt.close()

        # Simple 2D PDFs for quality instead of area, w/o area-weighting
        plot_histogram2d(self.error, self.quality, self.nbins, nonlin=True,
                         range=((np.min(self.error), np.max(self.error)),
                                (40, 1000)
                                ))
        plt.ylabel("Mesh Quality")
        plt.savefig(
            f'{self.pipeline.basename}-quality-error-pdf.{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

    def process(self):
        self._compute()
        self._render()


class ErrorReproject(Scene):
    def __init__(self, project_fun, *args, **kwargs):
        self.project_fun = project_fun

        super().__init__(*args, **kwargs)

    def process(self):
        self.output_data = self.project_fun(self.data)

        # Useful attributes
        self.A = self.output_data['Area']
        self.H = self.output_data['Mean Curvature']
        self.G = self.output_data['Gauss Curvature']
        # Instead of using reference data we need to ship another function
        # that estimates L1 error. Maybe putting the same ErrorMapScene but
        # with a different error_fun
        H_L1_error = np.sum(
            np.multiply(self.A,
                        np.abs(self.H - self.pipeline.H_def) / np.sum(self.A)))
        G_L1_error = np.sum(
            np.multiply(self.A,
                        np.abs(self.G - self.pipeline.G_def) / np.sum(self.A)))

        # Store global error in the global state
        # I need to use a temp dict instead of the mp proxy doesn't propagate
        # nested dicts
        temp_storage = dict(self.pipeline.storage)
        temp_storage[self.pipeline.filename].update(
            {"H L1 Error (Proj)": H_L1_error},
            {"G L1 Error (Proj)": G_L1_error}
        )
        self.pipeline.storage.update(temp_storage)


class FilterOutliersScene(Scene):
    def _compute(self):
        H = self.data['Mean Curvature']
        G = self.data['Gauss Curvature']

        inliers_i = np.where(H**2 >= G)
        inliers = self.data[inliers_i]
        inl_ratio = len(inliers)/len(H)
        outl_ratio = 1 - inl_ratio

        # Store stats in the global state
        # I need to use a temp dict instead of the mp proxy doesn't propagate
        # nested dicts
        temp_storage = dict(self.pipeline.storage)
        temp_storage[self.pipeline.filename].update({
            'Outliers Ratio': outl_ratio,
            'Number of Points': len(H)
        })
        self.pipeline.storage.update(temp_storage)

        self.output_data = inliers

    def process(self):
        self._compute()


class HGMapsScene(Scene):
    """ This scene generates the H-G maps """

    def _render(self):
        # Useful renamings
        self.A = self.data['Area']
        self.G = self.data['Gauss Curvature']
        self.H = self.data['Mean Curvature']

        r = self.data.shape[0]
        self.nbins = int(np.round(r**0.4))

        # PDF
        HGAbins, HAedges, GAedges = np.histogram2d(
            self.H/self.pipeline.H_def, self.G/self.pipeline.G_def,
            bins=self.nbins, weights=self.A/np.sum(self.A),
            range=((0, 2), (-1, 2)))

        HAc = (HAedges[:-1] + HAedges[1:])/2
        GAc = (GAedges[:-1] + GAedges[1:])/2

        plt.figure()
        cs = plt.contourf(GAc, HAc, HGAbins, levels=100)
        plt.xlabel(r'$\frac{G}{G_{ref}}$')
        plt.ylabel(r'$\frac{H}{H_{ref}}$')
        plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
        plt.colorbar(cs)
        plt.tight_layout()

        plt.savefig(
            f'{self.pipeline.basename}-area-weighted-pdf.{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

        # PDF w/o area weights
        HGbins, Hedges, Gedges = np.histogram2d(
            self.H/self.pipeline.H_def, self.G/self.pipeline.G_def,
            bins=self.nbins, range=((0, 2), (-1, 2)))

        Hc = (Hedges[:-1] + Hedges[1:])/2
        Gc = (Gedges[:-1] + Gedges[1:])/2

        plt.figure()
        cs = plt.contourf(Gc, Hc, HGbins, levels=100)
        plt.xlabel(r'$\frac{G}{G_{ref}}$')
        plt.ylabel(r'$\frac{H}{H_{ref}}$')
        plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
        plt.colorbar(cs)
        plt.tight_layout()

        plt.savefig(f'{self.pipeline.basename}-pdf.{self.pipeline.fmt}',
                    format=self.pipeline.fmt, dpi=300)
        plt.close()

        # We actually do not do anything with data
        self.output_data = self.data

    def process(self):
        self._render()


class WilmoreEnergyScene(Scene):
    """ This scene generates the Wilmore's energy  maps """

    def _render(self):
        # Useful renamings
        self.A = self.data['Area']
        self.G = self.data['Gauss Curvature']
        self.H = self.data['Mean Curvature']
        H_def = self.pipeline.H_def
        G_def = self.pipeline.G_def
        H = self.H/H_def
        G = self.G/G_def

        # Wilmore Energy (Normalized)
        W = H*H - G

        r = self.data.shape[0]
        self.nbins = int(np.round(r**0.4))

        # PDF
        WAbins, Wedges, Aedges = np.histogram2d(
            W, self.A,
            bins=self.nbins, weights=self.A/np.sum(self.A),
            range=((-0.5, 5), (np.min(self.A), np.max(self.A)))
        )

        Wc = (Wedges[:-1] + Wedges[1:])/2
        Ac = (Aedges[:-1] + Aedges[1:])/2

        plt.figure()
        cs = plt.contourf(Ac, Wc, WAbins, levels=100)
        plt.xlabel('Cell Area')
        plt.ylabel(r'$\frac{W}{W_{ref}}(H^2 - G)$')
        plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
        plt.colorbar(cs)
        plt.tight_layout()

        plt.savefig(
            f'{self.pipeline.basename}-area-weighted-wilmore-pdf.'
            f'{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

        # PDF w/o area weights
        WAbins, Wedges, Aedges = np.histogram2d(
            W, self.A, bins=self.nbins,
            range=((-0.5, 5), (np.min(self.A), np.max(self.A)))
        )

        Wc = (Wedges[:-1] + Wedges[1:])/2
        Ac = (Aedges[:-1] + Aedges[1:])/2

        plt.figure()
        cs = plt.contourf(Ac, Wc, WAbins, levels=100)
        plt.xlabel('Cell Area')
        plt.ylabel(r'$\frac{W}{W_{ref}}(H^2 - G)$')
        plt.contour(cs, colors='k', linewidths=0.5, levels=cs.levels[1::5])
        plt.colorbar(cs)
        plt.tight_layout()

        plt.savefig(
            f'{self.pipeline.basename}-wilmore-pdf.'
            f'{self.pipeline.fmt}',
            format=self.pipeline.fmt, dpi=300)
        plt.close()

        # We actually do not do anything with data
        self.output_data = self.data

    def process(self):
        self._render()
