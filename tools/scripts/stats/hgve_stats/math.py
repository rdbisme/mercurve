""" Useful mathematical functions """
import numpy as np


def nonlinspace(xmin: float, xmax: float, numsteps: int, spacing: float = 2)\
        -> np.ndarray:
    """ This function generates a non-linearly-spaced array from xmin to xmax
    with more points in the interval [0-1].

    Args:
        xmin(float): Starting value of the array
        xmax(float): Ending value of the array
        numsteps(int): Number of array elements
        spacing(float): If > 1, points are accumulated near xmin, if < 0
            they accumulate near xmax. If = 1 evenly spaced.

    Returns:
        array(np.ndarray): The non-linearly-spaced-array
    """

    span = (xmax-xmin)
    dx = 1.0 / (numsteps-1)
    return [xmin + (i*dx)**spacing*span for i in range(numsteps)]
