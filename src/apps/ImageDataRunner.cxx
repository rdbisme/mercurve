#include <iostream>
#include <numeric>

#include "filesystem.h"

#include "BlockMerger.h"
#include "BlockMirror.h"
#include "ImageDataRunner.h"
#include "Volume.h"

namespace hgve {

void ImageDataRunner::Run(docopt::Options args) {
  // Parse arguments
  auto filepath = fs::path(args["<file>"].asString());
  auto outputDir = fs::path(args["--output_dir"].asString());
  auto radiusFlag = args["--average"];
  auto writeStats = args["--write_stats"].asBool();
  auto mergeOnly = args["--merge-only"].asBool();
  auto mirrorAxes = args["--mirror"];
  auto doSanitize = args["--sanitize"].asBool();

  std::cout << "Processing vtkImageData file: " << filepath << std::endl;

  Volume volume(m_imageData);

  // Mirroring
  if (mirrorAxes.asStringList().size() > 0) {
    vtkSmartPointer<vtkImageData> mergingBlock = volume.GetOutput();
    std::vector<GridBlock> blocks;
    blocks.reserve(2);

    for (const auto &axString : mirrorAxes.asStringList()) {
      std::cout << "Mirroring about axis " << axString << std::endl;

      Direction axis;
      Orientation orientation;
      std::tie(axis, orientation) = axes.at(axString);

      BlockMirror mirror(mergingBlock, axis, orientation);
      mirror.Mirror();

      blocks = {{GridBlock(mergingBlock)}, {GridBlock(mirror.GetOutput())}};

      BlockMerger merger(blocks);
      merger.SetBoundariesOverlapping(false);
      merger.Merge();
      mergingBlock = merger.GetOutput();
    }

    volume.SetInputData(mergingBlock);
  }

  auto filename = filepath.stem();

  if (mergeOnly) {
    auto outputFilePath = outputDir / (filename.generic_string() + ".vti");
    volume.Write(outputFilePath.string());
    std::cout << "Output File: " << outputFilePath << std::endl;
    return;
  }

  // Compute average spacing
  auto spacing = volume.GetSpacing();
  auto dx = std::accumulate(spacing.begin(), spacing.end(), 0.0);

  dx /= spacing.size();

  auto surface = volume.Triangulate();

  // We exploit the spacing to set the Sanitize tolerance
  surface->ComputeCurvatures();
  if (doSanitize) {
    surface->Clean(0.01 * dx);
    surface->Sanitize();
  }

  // Average
  if (radiusFlag) {
    double radius = std::stod(radiusFlag.asString());
    std::cout << "The average spacing of the grid is: " << dx << std::endl;

    radius *= dx;

    std::cout << "The averaging radius is: " << radius << std::endl;

    surface->Average(radius);
  }

  // Output path
  auto outputFilePath = outputDir / (filename.generic_string() + ".vtp");

  std::cout << "Output File: " << outputFilePath << std::endl;
  surface->Write(outputFilePath.string());

  if (writeStats) {
    auto outputFilePath = outputDir / (filename.generic_string() + ".dat");
    surface->GetCurvArray().Write(outputFilePath.string());
  }
}

}  // namespace hgve
