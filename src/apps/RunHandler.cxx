#include <docopt.h>
#include <vtkImageData.h>
#include <vtkInformation.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <stdexcept>

#include "ErrorObserver.h"
#include "ImageDataRunner.h"
#include "MultiBlockRunner.h"
#include "RunHandler.h"

namespace hgve {

RunHandler::RunHandler(fs::path file) {
  // Reader
  m_reader = vtkSmartPointer<vtkXdmf3Reader>::New();

  // Add Error Handling
  vtkSmartPointer<ErrorObserver> errObs = vtkSmartPointer<ErrorObserver>::New();
  m_reader->AddObserver(vtkCommand::ErrorEvent, errObs);
  m_reader->AddObserver(vtkCommand::WarningEvent, errObs);

  // Read file
  m_reader->SetFileName(file.c_str());
  m_reader->Update();

  if (errObs->GetError()) {
    throw std::runtime_error(
        "Impossible to read the file. An error "
        "occurred. \n" +
        std::string(errObs->GetErrorMessage()));
  }

  // Check if the read file is a Time series (i.e. stores time steps)
  auto outInfo = m_reader->GetOutputInformation(0);
  double *timeSteps =
      outInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS());

  // And in that case store a vector of time steps
  if (timeSteps != nullptr) {
    auto len = outInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());

    m_timeSteps.assign(timeSteps, timeSteps + len);
  }
}

void RunHandler::RunOneTimeStep(vtkSmartPointer<vtkDataObject> input,
                                docopt::Options args) {
  // Try to Downcast the data object to see if it's a
  // vtkMultiBlockDataSet
  vtkSmartPointer<vtkMultiBlockDataSet> multiblock =
      vtkMultiBlockDataSet::SafeDownCast(input);

  std::unique_ptr<Runner> runner;

  // TODO: This can be maybe replaced by a visitor pattern
  if (multiblock != nullptr) {
    // Multiblock dataset
    std::cout << "A vtkMultiBlockDataSet has been provided..."
              << "\n";
    runner = std::unique_ptr<Runner>(new MultiBlockRunner(multiblock));
  } else {
    vtkSmartPointer<vtkImageData> imageData = vtkImageData::SafeDownCast(input);

    if (imageData == nullptr)
      throw std::runtime_error(
          "Input file not supported: it's not "
          "a composite dataset, but it is also not a "
          "vtkImageData");

    // ImageData only is supported for now as single block grid
    std::cout << "A vtkImageData grid has been provided"
              << "\n";

    runner = std::unique_ptr<Runner>(new ImageDataRunner(imageData));
  }

  runner->Run(args);
}

void RunHandler::Run(docopt::Options args) {
  if (m_timeSteps.empty()) {
    // Just one time-step
    this->RunOneTimeStep(m_reader->GetOutputDataObject(0), args);
  }

  else {
    // Iterate over the time steps. Each block of the output file is one
    // time step
    vtkSmartPointer<vtkMultiBlockDataSet> output =
        vtkMultiBlockDataSet::SafeDownCast(m_reader->GetOutputDataObject(0));

    auto filepath = fs::path(args["<file>"].asString());
    auto filename = filepath.stem();
    auto ext = filepath.extension();
    auto parent = filepath.parent_path();

    std::cout << "Input file is a time series... \n";
    for (unsigned int i = 0; i != m_timeSteps.size(); i++) {
      std::cout << "Post processing time step #" << i
                << ", t: " << m_timeSteps[i] << "\n";

      // Change the filename appending the time step
      fs::path newfilename = filename.generic_string() + "-" +
                             std::to_string(i) + ext.generic_string();
      args["<file>"] = docopt::value((parent / newfilename).generic_string());

      this->RunOneTimeStep(output->GetBlock(i), args);

      std::cout << "\n";
    }
  }
}

}  // namespace hgve
