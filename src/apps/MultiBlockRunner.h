#ifndef MULTIBLOCKRUNNER_H
#define MULTIBLOCKRUNNER_H

#include <vtkMultiBlockDataSet.h>

#include "BlockMerger.h"
#include "ImageDataRunner.h"

namespace hgve {
/** @brief A runner that takes a vtkMultiBlockDataSet and merges it into a
 * vtkImageData before running ImageDataRunner::Run
 */
class MultiBlockRunner : public ImageDataRunner {
 private:
  BlockMerger m_merger;

 public:
  MultiBlockRunner(vtkSmartPointer<vtkMultiBlockDataSet> grid);

  void Run(docopt::Options) override;

  ~MultiBlockRunner() {};
};
}  // namespace hgve

#endif /* MULTIBLOCKRUNNER_H */
