#ifndef RUNNER_H
#define RUNNER_H

#include <docopt.h>
#include <vtkDataObject.h>
#include <vtkSmartPointer.h>

namespace hgve {

/** @brief A Base class responsible or running the algorithm for the entry
 * point
 *
 * Concrete classes are implemented per each type of possible input
 * vtkDataObject retrieved with vtkXdmf3Reader
 */
class Runner {
 public:
  virtual void Run(docopt::Options) = 0;

  virtual ~Runner() {} ;

};

}  // namespace hgve

#endif /* RUNNER_H */
