/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POINT_H
#define POINT_H

#include <array>
#include <cmath>
#include <vector>

#include "CurvaturesData.h"
#include "ItemHash.h"
#include "Iterator.h"
#include "types.h"

namespace hgve {

// Forward declarations
class Cell;
class Edge;
class Surface;
class SimpleVector;

/**
 * @author RdB
 * @brief A class representing a Surface point in 3D
 * @todo Making this class usable for both Surface and Volume classes
 */
class Point {
  friend class Surface;
  friend class Iterator<Point>;

 private:
  /** @brief Static member to return the last Point present in the
   * given Surface object
   */
  static Point GetEndItem(Surface &mesh);

  // :: Attributes :: //
  IdType m_id;
  Surface &m_mesh;

 public:
  /**
   * @brief Constructor that just holds a reference to the mesh
   */
  Point(Surface &mesh) : m_mesh(mesh){};

  /** @brief Constructor for point ID
   */
  Point(Surface &mesh, IdType pointId) : m_id(pointId), m_mesh(mesh){};

  /** @brief copy constructor
   */
  Point(const Point &other) : m_id(other.m_id), m_mesh(other.m_mesh){};

  /** @brief Get the x, y, z coordinates of the Point
   *  @returns A Coords3D containing the coordinates
   */
  ThreeArray<double> GetCoordinates() const;

  /** @brief Access id of the point
   */
  IdType Id() const { return m_id; }

  /** @brief Compute the Euclidean norm of the Point
   *  @returns The value of the norm
   */
  double Norm() const;

  /** @brief Iterate over the cells that share this point
   *  @returns An iterable over all the cells that compose the one ring
   *      of this point (i.e. share this point)
   */

  Set<Cell> OneRingCells() const;

  /** @brief Iterate over the connectivity of the one-ring of this point
   *  @returns An iterable over all the points that are connected by an
   *      edge to this point
   */

  Set<Point> OneRingPoints() const;

  /**
   * @brief Iterate over the edges that compose the 1-ring
   * @returns An iterable over the edges that are connected to this point
   */
  Set<Edge> OneRingEdges() const;

  /** @brief Compute the 1-ring area
   *
   * It uses the mixed area approach described in @cite
   * meyer_discrete_2003.
   * @attention Not really used in the computation of the mean curvature
   * to avoid an additional loop.
   * @returns The value of the 1-ring area
   */
  double OneRingArea() const;

  /** @brief Compute the 1-ring geometrical properties
   *
   * The mean curvature is computed as a space-averaged value on the
   * point 1-ring as explained in @cite meyer_discrete_2003
   * @returns The values of the properties associated to each 1-ring
   */
  CurvaturesData OneRingProperties() const;

  // :: Operators ::
  SimpleVector operator+(const Point &) const;
  SimpleVector operator-(const Point &) const;
  SimpleVector operator^(const Point &) const;

  Point &operator=(const Point &other);

  Point &operator++() {
    m_id++;
    return *this;
  }
  Point operator++(int) {
    Point previous(m_mesh, m_id);
    this->operator++();
    return previous;
  }
  bool operator==(const Point &other) const { return m_id == other.m_id; }
  bool operator!=(const Point &other) const { return m_id != other.m_id; }
};
}  // namespace hgve
#endif /* POINT_H */
