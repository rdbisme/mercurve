/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIMPLEVECTOR_H
#define SIMPLEVECTOR_H

#include "Point.h"
#include "types.h"

namespace hgve {
/**
 * @author RdB
 * @brief A class representing the sum/difference between Point
 */
class SimpleVector {
 private:
  double m_x, m_y, m_z;

 public:
  /** @brief Constructor from 3D position vector */
  SimpleVector(ThreeArray<double> p);

  /** @brief Constructor from separate x, y, z coordinates */
  SimpleVector(double x, double y, double z) : m_x(x), m_y(y), m_z(z){};

  /** @brief Accessor to X */
  double X() const { return m_x; }

  /** @brief Accessor to Y */
  double Y() const { return m_y; }

  /** @brief Accessor to Z */
  double Z() const { return m_z; }

  /** @brief Compute square of the Norm */
  double Norm2() const;

  /** @brief Compute the Norm */
  double Norm() const;

  /** @brief Scalar Product */
  double Dot(const SimpleVector &b) const;

  /** @brief Element-wise Multiplication */
  SimpleVector Multiply(const SimpleVector &b) const;

  /** @brief Element-wise Division */
  SimpleVector Divide(const SimpleVector &b) const;

  /** @brief Element-wise std::min equivalent */
  friend SimpleVector Min(const SimpleVector &a, const SimpleVector &b);

  /** @brief Element-wise std::max equivalent */
  friend SimpleVector Max(const SimpleVector &a, const SimpleVector &b);

  /** @brief Clamp function for vectors as for GLSL language
   *
   * The clamp function returns x if it is larger than minVal and smaller
   * than maxVal. In case x is smaller than minVal, minVal is returned.
   * If x is larger than maxVal, maxVal is returned.
   * The operation is done component-wise.
   *
   * @param   X   The value to clamp
   * @param   minVal  The minimum value to clamp to
   * @param   maxVal  The maximum value to clamp to
   * @returns A clamped SimpleVector
   */
  SimpleVector Clamp(const SimpleVector &minVal,
                     const SimpleVector &maxVal) const;

  /** @brief Element-wise absolute value
   */
  SimpleVector Abs() const;

  // :: Operators ::
  friend SimpleVector operator+(const SimpleVector &a, const SimpleVector &b);
  SimpleVector &operator+=(const SimpleVector &);

  friend SimpleVector operator-(const SimpleVector &a, const SimpleVector &b);
  SimpleVector &operator-=(const SimpleVector &);
  SimpleVector operator-() const &;

  /* Multiplication by a scalar */
  friend SimpleVector operator*(const double lambda, const SimpleVector &v);
  friend SimpleVector operator*(const SimpleVector &v, const double lambda);
  SimpleVector &operator*=(const double lambda);

  /* Division by a scalar */
  friend SimpleVector operator/(const double lambda, const SimpleVector &v);
  friend SimpleVector operator/(const SimpleVector &v, const double lambda);
  SimpleVector &operator/=(const double lambda);

  bool operator==(const SimpleVector &) const;
  bool operator!=(const SimpleVector &) const;

  /** @brief Cross Product */
  friend SimpleVector operator^(const SimpleVector &a, const SimpleVector &b);
  SimpleVector operator^(const SimpleVector &);
};
}  // namespace hgve

#endif /* SIMPLEVECTOR_H */
