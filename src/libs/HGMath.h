/** Useful math stuff */

#ifndef MATH_H
#define MATH_H

#include <vtkMath.h>
#include <limits>

namespace hgve {

/***************
 *  Constants  *
 ***************/
static const double TWO_PI = 2 * vtkMath::Pi();

/************
 *  Limits  *
 ************/
static const double EPS = std::numeric_limits<double>::epsilon();
static const double SMALL = 50 * std::numeric_limits<double>::epsilon();
static const double maxPrecision = std::numeric_limits<double>::max_digits10;

/****************
 *  IO Numbers  *
 ****************/
// Sets the various padding and width to use when serializing data into
// a file using std::cout
static const int TAB_WIDTH = 2;
static const int ID_WIDTH = 6;
static const int PRECISION_WIDTH = 24;

/********************
 *  Math Functions  *
 ********************/
template <typename T>
int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

}  // namespace hgve
#endif /* MATH_H */
