/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>

#include <vtkCleanPolyData.h>
#include <vtkPointData.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkZLibDataCompressor.h>

#include "Point.h"
#include "Surface.h"
#include "SurfaceWriterFactory.h"

namespace hgve {

// :: Public ::
Surface::Surface(vtkSmartPointer<vtkPolyData> polyPtr) : m_polyPtr(polyPtr) {
  this->Rebuild();
}

Surface::Surface(const Surface &other) { Surface(other.m_polyPtr); }

void Surface::Clean(double rad) {
  // Merge near points
  vtkSmartPointer<vtkCleanPolyData> cleaner =
      vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInputData(m_polyPtr);
  cleaner->ToleranceIsAbsoluteOn();
  cleaner->SetAbsoluteTolerance(rad);
  cleaner->ConvertPolysToLinesOn();
  cleaner->ConvertLinesToPointsOn();
  cleaner->ConvertStripsToPolysOn();
  cleaner->PointMergingOn();
  cleaner->Update();

  m_polyPtr = cleaner->GetOutput();

  this->Rebuild();
}

void Surface::Sanitize() {
  // Can't use the iterator since I'm modifying the underlying low-level
  //  structure
  for (vtkIdType i = 0; i < m_polyPtr->GetNumberOfCells(); i++) {
    IdType nPoints;
    IdType *pts;
    m_polyPtr->GetCellPoints(i, nPoints, pts);

    if (nPoints != 3) m_polyPtr->DeleteCell(i);
  }

  m_polyPtr->RemoveDeletedCells();
  this->Rebuild();
}

void Surface::Rebuild() {
  if (m_polyPtr->NeedToBuildCells()) {
    m_polyPtr->BuildCells();
  }

  m_polyPtr->BuildLinks();
}

ThreeArray<double> Surface::GetPoint(IdType id) const {
  double coords[3];
  ThreeArray<double> ret_coords;
  m_polyPtr->GetPoint(id, coords);

  // Copy the double array to the std::array
  std::copy(std::begin(coords), std::end(coords), std::begin(ret_coords));
  return ret_coords;
}

vtkIdType Surface::FindPoint(double x, double y, double z) const {
  vtkIdType pointId;
  pointId = m_polyPtr->FindPoint(x, y, z);

  return pointId;
}

vtkIdType Surface::GetNumberOfPoints() const {
  return m_polyPtr->GetNumberOfPoints();
}

vtkIdType Surface::GetNumberOfCells() const {
  return m_polyPtr->GetNumberOfCells();
}

ThreeArray<Point> Surface::GetCellPoints(const Cell cell) {
  IdType nPoints;
  IdType *pts;

  m_polyPtr->GetCellPoints(cell.m_id, nPoints, pts);

  if (nPoints != 3) {
    std::stringstream error_msg;
    error_msg << "Cell " << cell.m_id << " "
              << " has " << nPoints
              << " vertices. We can only handle triangles "
                 "for the moment";
    throw std::runtime_error(error_msg.str());
  }

  return {{
      Point(*this, pts[0]),
      Point(*this, pts[1]),
      Point(*this, pts[2]),
  }};
}

Set<Cell> Surface::GetPointCells(const Point &point) {
  IdType *cells;
  unsigned short ncells;
  Set<Cell> cellList;

  m_polyPtr->GetPointCells(point.m_id, ncells, cells);

  for (IdType i = 0; i < ncells; i++) {
    Cell currCell(*this, cells[i]);
    cellList.insert(currCell);
  }

  return cellList;
}

double Surface::Area() {
  double A = 0;
  // Iterate over cells summing up their area
  for (auto c : this->IterateOver<Cell>()) {
    A += c.Area();
  }

  return A;
}

void Surface::ComputeCurvatures() {
  m_hasGeometricalData = true;

  // Pre-allocate space
  IdType nPoints = this->GetNumberOfPoints();
  m_curvData.SetNumberOfValues(nPoints);

  for (auto p : this->IterateOver<Point>()) {
    try {
      auto curvatures = p.OneRingProperties();
      m_curvData.Append(p.Id(), curvatures);
    } catch (std::runtime_error &e) {
      // Getting strange cells, we do nothing since we operate
      // only on triangles
      continue;
    };
  }
}

void Surface::Average(double radius) {
  m_locator->SetDataSet(m_polyPtr);
  m_locator->BuildLocator();

  // I need to store temporarily the averaged data
  // to avoid messing up when averagin neighbour points of a point
  CurvaturesDataArray tmpCurvData;
  tmpCurvData.SetNumberOfValues(this->GetNumberOfPoints());

  for (auto p : this->IterateOver<Point>()) {
    vtkSmartPointer<vtkIdList> neighbours = vtkSmartPointer<vtkIdList>::New();

    m_locator->FindPointsWithinRadius(radius, p.GetCoordinates().data(),
                                      neighbours);

    // Temp stock this point area
    double curPointArea = m_curvData.Get(p.Id()).A();
    CurvaturesData averagedCurv;

    // This ID list stores the neighbours of a neighbour
    vtkSmartPointer<vtkIdList> neighNeighbours =
        vtkSmartPointer<vtkIdList>::New();

    // Loop over the neighbours IDs
    for (IdType i = 0; i < neighbours->GetNumberOfIds(); i++) {
      // Neighbour point
      Point curNeigh(*this, neighbours->GetId(i));

      // Find all the neighbours of `curNeigh`
      m_locator->FindPointsWithinRadius(
          radius, curNeigh.GetCoordinates().data(), neighNeighbours);

      // Sum up the area of the neighbours of curNeigh
      double neighArea = 0;
      for (IdType j = 0; j < neighNeighbours->GetNumberOfIds(); j++) {
        auto curv = m_curvData.Get(neighNeighbours->GetId(j));

        // Get Area since we want to average by Area
        double A = curv["Area"];

        neighArea += A;
      }

      auto curv = m_curvData.Get(neighbours->GetId(i));

      // Extract Area from the curvature values
      double A = curv["Area"];

      // Accumulate the values for the neighbours
      // and divide by the sum of the areas of the neighbours of
      // this neighbour (Sk on the IJMF paper)
      averagedCurv += curv * A / neighArea;
    }

    // I do not want the area changed
    averagedCurv.A() = curPointArea;

    // Append the new value
    tmpCurvData.Append(p.Id(), averagedCurv);
  }

  m_curvData = tmpCurvData;
}

void Surface::Write(std::string filename) {
  SurfaceWriterFactory::GetInstance().Write(filename, *this);
}

// :: Operator ::
Surface &Surface::operator=(const Surface &other) {
  m_polyPtr = other.m_polyPtr;
  return *this;
}
}  // namespace hgve
