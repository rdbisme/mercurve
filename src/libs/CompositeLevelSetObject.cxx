/* Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CompositeLevelSetObject.h"
#include "LevelSetObject.h"

namespace hgve {

double noop(double first_sdf, double second_sdf) { return first_sdf; }

CompositeLevelSetObject::CompositeLevelSetObject(LevelSetObjectPtr first,
                                                 LevelSetObjectPtr second,
                                                 CompositeFun *fun)
    : m_first(first), m_second(second), m_fun(fun) {}

CompositeLevelSetObject::CompositeLevelSetObject(
    const CompositeLevelSetObject &other)
    : m_first(other.m_first), m_second(other.m_second), m_fun(other.m_fun) {}

double CompositeLevelSetObject::SDF(double x, double y, double z) const {
  return m_fun(m_first->SDF(x, y, z), m_second->SDF(x, y, z));
}

std::unique_ptr<LevelSetObject> CompositeLevelSetObject::clone() const {
  auto clone = std::unique_ptr<LevelSetObject>(
      new CompositeLevelSetObject(this->m_first, this->m_second, this->m_fun));

  return clone;
}

CompositeLevelSetObject &CompositeLevelSetObject::operator=(
    CompositeLevelSetObject &&other) {
  m_first = std::move(other.m_first);
  m_second = std::move(other.m_second);
  m_fun = other.m_fun;

  return *this;
}

}  // namespace hgve
