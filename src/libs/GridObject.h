/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRIDOBJECT_H
#define GRIDOBJECT_H

namespace hgve {
/**
 * @brief An interface representing the possible input objects for a grid
 *
 * At the moment the results of a DNS could be given as different datasets
 * i.e. a vtkMultiBlockDataSet that needs to be merged, or a bare
 * vtkImageData that can be used as it is, and maybe in the future other
 * types of objects. This provides an interface that allows polymorphims
 * notably within Volume class to allow to use different inputs.
 */
class GridObject {
 private:
 public:
  GridObject();
  virtual ~GridObject();
};

#endif /* GRIDOBJECT_H */
}
