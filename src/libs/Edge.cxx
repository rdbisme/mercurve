/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Edge.h"

namespace hgve {

IdType Edge::Id() const {
  size_t idc = std::hash<IdType>{}(m_pc.Id());
  size_t id2 = std::hash<IdType>{}(m_p2.Id());

  return idc ^= id2 + 0x9e3779b9 + (idc << 6) + (idc >> 2);
}

Pair<Cell> Edge::EdgeCells() { return m_cellEdgePair; }
}  // namespace hgve
