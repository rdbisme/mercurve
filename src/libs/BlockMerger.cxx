/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iterator>

#include "BlockMerger.h"
#include "HGMath.h"

namespace hgve {

void BlockMerger::SortBlocks() {
  std::sort(m_blocks.begin(), m_blocks.end(),
            [](const GridBlock a, const GridBlock b) -> bool { return a < b; });
}

BlockMerger::BlockMerger(std::vector<GridBlock> blocks)
    : m_mergedImageData(vtkSmartPointer<vtkImageData>::New()) {
  m_blocks = blocks;
  this->SortBlocks();
}

BlockMerger::BlockMerger(vtkSmartPointer<vtkMultiBlockDataSet> multiblock)
    : m_mergedImageData(vtkSmartPointer<vtkImageData>::New()) {
  m_allocatedBlocks = multiblock->GetNumberOfBlocks();
  m_blocks.reserve(m_allocatedBlocks);

  // Iterate over blocks
  for (IdType i = 0; i < m_allocatedBlocks; i++) {
    // Get vtkImageData block
    vtkSmartPointer<vtkImageData> block =
        vtkImageData::SafeDownCast(multiblock->GetBlock(i));

    m_blocks.push_back(GridBlock(block));
  }

  this->SortBlocks();
}

void BlockMerger::InsertBlock(GridBlock block) const {
  auto sijk = block.GetStartIJK();
  auto dims = block.GetDimensions();

  if (m_overlapping) {
    // If we set the overlapping blocks, we skip the first
    // layer of points
    // Starting from second element on each axis
    for (IdType i = 1; i < dims[0] - 1; i++) {
      for (IdType j = 1; j < dims[1] - 1; j++) {
        for (IdType k = 1; k < dims[2] - 1; k++) {
          double value = block.GetScalarComponent(i, j, k, m_levelSetComponent);

          // If overlapping we need to apply an offset when
          // putting data in the merged ImageData
          m_mergedImageData->SetScalarComponentFromDouble(
              sijk[0] + i - 1, sijk[1] + j - 1, sijk[2] + k - 1,
              m_levelSetComponent, value);
        }
      }
    }
  } else {
    for (IdType i = 0; i < dims[0]; i++) {
      for (IdType j = 0; j < dims[1]; j++) {
        for (IdType k = 0; k < dims[2]; k++) {
          double value = block.GetScalarComponent(i, j, k, m_levelSetComponent);

          m_mergedImageData->SetScalarComponentFromDouble(
              sijk[0] + i, sijk[1] + j, sijk[2] + k, m_levelSetComponent,
              value);
        }
      }
    }
  }
}

void BlockMerger::Merge() {
  // Starting block
  GridBlock &bl0 = m_blocks[0];
  bl0.SetStartIJK(0, 0, 0);

  auto dims = bl0.GetDimensions();

  if (m_overlapping) {
    for (auto &dim : dims) {
      dim -= 2;
    }
  }

  IdType nx = dims[0];
  IdType ny = dims[1];
  IdType nz = dims[2];
  bl0.SetEndIJK(nx, ny, nz);

  GridBlock cur_blk_x = bl0;
  GridBlock cur_blk_y = bl0;
  GridBlock cur_blk_z = bl0;

  bool wrapX = false;
  bool wrapY = false;

  // Loop on the other blocks and assess along which axis to merge
  // and how many elements we need to allocate
  for (auto blkItr = std::next(m_blocks.begin()); blkItr != m_blocks.end();
       ++blkItr) {
    GridBlock &block = *blkItr;

    // Handle x-coordinate wrapping
    double *spacing = block.GetSpacing();

    if (std::abs(block.X() - cur_blk_x.X()) / spacing[0] > SMALL) {
      auto sijk = cur_blk_x.GetStartIJK();
      auto eijk = cur_blk_x.GetEndIJK();

      auto dims = block.GetDimensions();

      if (m_overlapping) {
        for (auto &dim : dims) {
          dim -= 2;
        }
      }

      nx += dims[0];

      block.SetStartIJK(eijk[0], sijk[1], sijk[2]);
      block.SetEndIJK(eijk[0] + dims[0], sijk[1] + dims[1], sijk[2] + dims[2]);

      wrapX = true;

      cur_blk_z = block;
      cur_blk_y = block;
      cur_blk_x = block;

      continue;
    }

    // Handle y-coordinate wrapping
    else if (std::abs(block.Y() - cur_blk_y.Y()) / spacing[1] > SMALL) {
      auto sijk = cur_blk_y.GetStartIJK();
      auto eijk = cur_blk_y.GetEndIJK();

      auto dims = block.GetDimensions();

      if (m_overlapping) {
        for (auto &dim : dims) {
          dim -= 2;
        }
      }

      if (!wrapX) ny += dims[1];

      block.SetStartIJK(sijk[0], eijk[1], sijk[2]);
      block.SetEndIJK(sijk[0] + dims[0], eijk[1] + dims[1], sijk[2] + dims[2]);

      wrapY = true;

      cur_blk_y = block;
      cur_blk_z = block;

      continue;
    }

    // Handle z-coordinate wrapping
    else {
      auto sijk = cur_blk_z.GetStartIJK();
      auto eijk = cur_blk_z.GetEndIJK();

      auto dims = block.GetDimensions();

      if (m_overlapping) {
        for (auto &dim : dims) {
          dim -= 2;
        }
      }

      if (!wrapY) nz += dims[2];

      block.SetStartIJK(sijk[0], sijk[1], eijk[2]);
      block.SetEndIJK(sijk[0] + dims[0], sijk[1] + dims[1], eijk[2] + dims[2]);

      cur_blk_z = block;
    }
  }

  // Allocate space now for the merged grid
  m_mergedImageData->SetDimensions(nx, ny, nz);
  m_mergedImageData->AllocateScalars(VTK_DOUBLE, 1);

  // Same origin and spacing as the first block
  m_mergedImageData->SetOrigin(bl0.GetOrigin());
  m_mergedImageData->SetSpacing(bl0.GetSpacing());

  // Insert first block
  this->InsertBlock(bl0);

  // Insert the other blocks
  for (auto blkItr = std::next(m_blocks.begin()); blkItr != m_blocks.end();
       ++blkItr) {
    this->InsertBlock(*blkItr);
  }
}

}  // namespace hgve
