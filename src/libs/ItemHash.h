/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ITEMHASH_H
#define ITEMHASH_H

namespace hgve {
/** @brief The hashing function for a general IterItem
 *
 * It simply has the object with its ID
 */
template <typename IterItem>
struct ItemHash {
  size_t operator()(const IterItem &cell) const { return cell.Id(); }
};

}  // namespace hgve

#endif /* ITEMHASH_H */
