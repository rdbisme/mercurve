/*
 * Mercur(v)e
 * Copyright © 2019 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLOCKMIRROR_H
#define BLOCKMIRROR_H

#include <unordered_map>

#include <vtkSmartPointer.h>

#include "GridBlock.h"

namespace hgve {
/** @brief Enum class to indicate the direction of mirroring */
enum class Direction { X = 0, Y = 1, Z = 2 };

/** @brief Enum class for the orientation of flipping
 *
 * You can mirror in the same orientation as the given axis or in
 * the opposite one
 */
enum class Orientation { POS = 1, NEG = -1 };

/** @brief A map used to match string representation of axes to the enum
 *
 * Mainly useful when parsing user input in the main file
 */
static std::unordered_map<std::string, std::pair<Direction, Orientation>> axes =
    {
        {"+X", std::make_pair(Direction::X, Orientation::POS)},
        {"+Y", std::make_pair(Direction::Y, Orientation::POS)},
        {"+Z", std::make_pair(Direction::Z, Orientation::POS)},
        {"-X", std::make_pair(Direction::X, Orientation::NEG)},
        {"-Y", std::make_pair(Direction::Y, Orientation::NEG)},
        {"-Z", std::make_pair(Direction::Z, Orientation::NEG)},
};

/** @brief A class responsible to reflect a vtkImageData along an axis
 *  @author RdB
 */
class BlockMirror {
 private:
  /** @brief The block to mirror **/
  vtkSmartPointer<vtkImageData> m_block;

  /** @brief The direction axis along which to reflect */
  Direction m_axis;

  /** @brief The orientation of flipping along the given axis */
  Orientation m_orientation;

 public:
  /** @brief Constructor from a vtkImageData and a Direction axis */
  BlockMirror(vtkSmartPointer<vtkImageData> block, Direction axis,
              Orientation orientation);

  /** @brief Set the axis along which to perform the reflection */
  void SetAxis(const Direction axis);

  /** @brief Perform the reflection
   *  @returns A GridBlock object that contains the mirrored image
   * */
  void Mirror();

  /** @brief Return flipped and merged block */
  vtkSmartPointer<vtkImageData> GetOutput() { return m_block; }
};
}  // namespace hgve

#endif /* BLOCKMIRROR_H */
