/* Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CompositeLevelSetObject.h"
#include "ElongatedLevelSetObject.h"

namespace hgve {

ElongatedLevelSetObject LevelSetObject::Elongate(const SimpleVector v) const {
  return {this->clone(), v};
}

CompositeLevelSetObject LevelSetObject::operator+(LevelSetObject &other) {
  CompositeFun *fun = [](double sdf_value1, double sdf_value2) {
    return std::min(sdf_value1, sdf_value2);
  };

  return {this->clone(), other.clone(), fun};
}

CompositeLevelSetObject LevelSetObject::operator-(LevelSetObject &other) {
  CompositeFun *fun = [](double sdf_value1, double sdf_value2) {
    return std::max(-sdf_value1, sdf_value2);
  };

  return {this->clone(), other.clone(), fun};
}

CompositeLevelSetObject LevelSetObject::operator^(LevelSetObject &other) {
  CompositeFun *fun = [](double sdf_value1, double sdf_value2) {
    return std::max(sdf_value1, sdf_value2);
  };

  return {this->clone(), other.clone(), fun};
}
}  // namespace hgve
