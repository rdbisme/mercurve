/* Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVELSETSPHERE_H
#define LEVELSETSPHERE_H

#include "LevelSetObject.h"
#include "SimpleVector.h"

namespace hgve {

/** @brief A sphere in \f$\mathbb{R}^3\f$ represented by its SDF
 *  @author RdB
 */
class LevelSetSphere : public LevelSetObject {
 private:
  double m_R;       /**< The radius of the sphere */
  SimpleVector m_C; /**< The center of the sphere */

 public:
  /** @brief Constructor
   *
   *  @param  radius      The radius of the sphere
   */
  LevelSetSphere(double radius, SimpleVector center);

  double SDF(double x, double y, double z) const override;
  std::unique_ptr<LevelSetObject> clone() const override;
};
}  // namespace hgve

#endif /* LEVELSETSPHERE_H */
