#include <iostream>

#include <vtkArrayData.h>
#include <vtkArrayWriter.h>

#include "CurvaturesDataArray.h"
#include "HGMath.h"

namespace hgve {
CurvaturesDataArray::CurvaturesDataArray() {
  for (const auto &field : CurvaturesData::m_names) {
    vtkSmartPointer<vtkDoubleArray> dataArray =
        vtkSmartPointer<vtkDoubleArray>::New();
    dataArray->SetName(field.c_str());
    m_data[field] = dataArray;
  }
}

void CurvaturesDataArray::SetNumberOfValues(IdType n) {
  for (const auto &field : CurvaturesData::m_names) {
    m_data[field]->SetNumberOfValues(n);
  }
  m_npoints = n;
}

void CurvaturesDataArray::Append(IdType i, CurvaturesData curvData) {
  for (auto &kv : curvData.m_fields) {
    m_data[kv.first]->SetValue(i, kv.second);
  }
}

CurvaturesData CurvaturesDataArray::Get(IdType i) const {
  CurvaturesData curv;
  for (const auto &kv : m_data) {
    curv[kv.first] = kv.second->GetValue(i);
  }

  return curv;
}

void CurvaturesDataArray::Write(std::string filename) const {
  ofstream file;
  file.open(filename);

  // Write the headers
  file << "# ";
  for (const auto &kv : m_data) {
    file << kv.first << std::setw(TAB_WIDTH) << " ";
  }

  file << "\n" << std::endl;

  // Write the data
  for (IdType i = 0; i < this->GetNumberOfElements(); i++) {
    for (const auto &kv : m_data) {
      file << std::right << std::setw(PRECISION_WIDTH)
           << std::setprecision(maxPrecision) << std::scientific
           << kv.second->GetValue(i) << std::setw(TAB_WIDTH) << " ";
    }
    file << std::endl;
  }

  file.close();
}

}  // namespace hgve
