#include <vtkPointData.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

#include "SurfaceWriter.h"

namespace hgve {
Surface &SurfaceWriter::RescaleSurface(Surface &surface) {
  if (surface.HasGeometricalData()) {
    // Add the arrays into the vtkPolyData
    for (auto &kv : surface.m_curvData) {
      surface.m_polyPtr->GetPointData()->AddArray(kv.second);
    }
  }

  return surface;
}

SurfaceWriter::SurfaceWriter(std::string filename, Surface &surface)
    : m_filename(filename), m_surface(SurfaceWriter::RescaleSurface(surface)) {}

}  // namespace hgve
