/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDGE_H
#define EDGE_H

#include "Cell.h"
#include "Point.h"
#include "types.h"

namespace hgve {
/**
 * @author RdB
 * @brief A class representing an edge
 *
 * This class represents an edge of a 1-ring. So, let's imagine the 1-ring
 * as a star:
 *
 *                               x     x
 *                                \   /
 *                                 \ /
 *                               x--C::::N
 *                                  |
 *                                  x
 * So each (x) is a point of the 1-ring, C is the vertex of the 1-ring, and
 * N is one neighbour. C-N is one edge, that is hence given by the center
 * point of the 1-ring (C) and one neighbour point.
 *
 * @attention It's important to specify which is the center point because
 * that point is used to lookup for the cells that share the edge
 */
class Edge {
 private:
  /** This is the point that is the center of the star*/
  Point m_pc;

  /**< This is a neighbour point. */
  Point m_p2;

  /** A pair containing the two cells that share the same */
  const Pair<Cell> m_cellEdgePair;

 public:
  /**
   * @brief Constructor from two Points and a Pair
   */
  Edge(Point pc, Point p2, Pair<Cell> cellEdgePair)
      : m_pc(pc), m_p2(p2), m_cellEdgePair(cellEdgePair){};

  /**
   * @brief Id of the Edge
   *
   * The Id is given by a combination of the Ids of the two points
   * composing the edge
   */
  IdType Id() const;

  /**
   * @brief Accessor to the central vertex
   */
  const Point &Pc() const { return m_pc; }

  /**
   * @brief Accessor to the "other" vertex
   */
  const Point &P2() const { return m_p2; }

  /**
   * @brief Access the two cells that share this edge
   * @returns A Pair<Cell> that contains the two cells
   */
  Pair<Cell> EdgeCells();

  // :: Operators ::
  bool operator==(const Edge &other) const { return this->Id() == other.Id(); }
};
}  // namespace hgve

#endif /* EDGE_H */
