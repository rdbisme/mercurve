/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRIDBLOCK_H
#define GRIDBLOCK_H

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

#include "types.h"

namespace hgve {
/**
 * @brief This class is a handy wrapper over a vtkImageData
 *
 * In order to be able to sort it coordinate-wise and perform the algorithm
 * to merge the blocks if a simulation is multiblock. A block is identified
 * by the coordinate of the leftmost bottom corner.
 */
class GridBlock {
 private:
  int m_nx; /**< Number of elements x-wise */
  int m_ny; /**< Number of elements y-wise */
  int m_nz; /**< Number of elements z-wise */

  double m_x; /**< x-coordinate of the bottom-left corner point */
  double m_y; /**< y-coordinate of the bottom-left corner point */
  double m_z; /**< z-coordinate of the bottom-left corner point */

  /* @brief Starting position in the merged block */
  std::vector<int> m_startijk;

  /* @brief Flag to check if m_startijk has been set */
  bool m_hasStartIJK = false;

  /* @brief Ending position in the merged block */
  std::vector<int> m_endijk;

  /* @brief Flag to check if m_endijk has been set */
  bool m_hasEndIJK = false;

  /** @brief The pointer to underlying vtkImageData */
  vtkSmartPointer<vtkImageData> m_block;

 public:
  GridBlock(vtkSmartPointer<vtkImageData> block);

  /** @brief Accessor method to the number of x-wise elements */
  int Nx() const { return m_nx; }

  /** @brief Accessor method to the number of y-wise elements */
  int Ny() const { return m_ny; }

  /** @brief Accessor method to the number of z-wise elements */
  int Nz() const { return m_nz; }

  /** @brief Returns the dimension over x, y, z axis respectively */
  std::vector<int> GetDimensions() const;

  /** @brief Returns the spacing of the block */
  double *GetSpacing() const;

  /** @brief Returns the Origin coordinates of the block */
  double *GetOrigin() const;

  /** @brief Accessor method to the x-coordinate of the block corner */
  double X() const { return m_x; }

  /** @brief Accessor method to the y-coordinate of the block corner */
  double Y() const { return m_y; }

  /** @brief Accessor method to the z-coordinate of the block corner */
  double Z() const { return m_z; }

  /** @brief Set the initial position of the block within the merged
   * grid */
  void SetStartIJK(int i, int j, int k);

  /** @brief Get the initial position of the block within the merged
   * grid */
  std::vector<int> GetStartIJK() const;

  /** @brief Set the final position of the block within the merged grid*/
  void SetEndIJK(int i, int j, int k);

  /** @brief Get the final position of the block within the merged grid*/
  std::vector<int> GetEndIJK() const;

  /** @brief Get the scalar component value at ijk
   *
   * @param[in] componentID   The index of the component to get
   * @param[in] i             The x-wise index of the block element
   * @param[in] j             The y-wise index of the block element
   * @param[in] k             The z-wise index of the element
   *
   * @returns The value of the scalar component identified by
   * `componentID` at i,j,k position in the grid
   */
  double GetScalarComponent(const int componentID, const int i, const int j,
                            const int k) const;

  bool operator<(const GridBlock &other) const;
};
}  // namespace hgve

#endif /* GRIDBLOCK_H */
