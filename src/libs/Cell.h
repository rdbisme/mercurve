/*
 * Mercur(v)e
 * Copyright © 2018 Ruben Di Battista
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CELL_H
#define CELL_H

#include <vtkSmartPointer.h>
#include <vtkTriangle.h>
#include <vtkType.h>
#include <array>
#include <cmath>

#include "ItemHash.h"
#include "Iterator.h"
#include "SimpleVector.h"
#include "types.h"

namespace hgve {

class Surface;  // Forward declaration

/**
 * @author RdB
 * @brief A class representing a Surface cell. It provides method
 * to extract important features of the cell
 */
class Cell {
 private:
  friend class Surface;
  friend class Iterator<Cell>;

  /** @brief Static member to return the last Cell present in the
   * given Surface object
   */
  static Cell GetEndItem(Surface &mesh);

  // :: Attributes :: //
  IdType m_id;     /**< The id of the cell */
  Surface &m_mesh; /**< A reference to the Surface */

  /** @brief Using vtkTriangle to compute useful properties of the cell*/
  vtkSmartPointer<vtkTriangle> m_triangle;

 public:
  /** @brief: Constructor from cell ID
   */
  Cell(Surface &mesh, IdType cellId);

  /** @brief m_id accessor
   */
  IdType Id() const { return m_id; }

  /** @brief Get the points that define the cell connectivity
   *  @returns An iterable containing the coordinates
   */
  ThreeArray<Point> Points() const;

  /** @brief Get the edges that connect the cell points
   *  @returns An iterable containing the edges
   */
  ThreeArray<SimpleVector> Edges() const;

  /** @brief Compute Surface Area of the cell
   *  @returns The value of the surface
   */
  double Area() const;

  /** @brief Compute the normal vector to the cell
   */
  SimpleVector Normal() const;

  /** @brief Compute the perimeter of the cell
   *  @returns The value of the perimeter
   */

  double Perimeter() const;

  /** @brief Compute the mixed area of the cell
   *  @param[in]  pc      The point that is the vertex of the 1-ring
   *
   * In @cite meyer_discrete_2003 they define a "special" area of a cell
   * when performing the calculations for a 1-ring. This area is the
   * Voronoi region if the triangle is non-obtuse, otherwise is a
   * fraction of the area of the triangle if one of the angles of is
   * obtuse
   * @returns The value of the mixed area
   */
  double MixedArea(const Point &pc) const;

  /** @brief Compute the cotangent of the angle that is comprised between
   * two edges
   * @param[in] a First Edge
   * @param[in] b Second Edge
   * @return The value of the cotangent
   */
  static double ComputeCotan(const SimpleVector &a, const SimpleVector &b);

  /** @brief Compute the contangent of the angle that is opposed to an
   *  edge
   *  @param[in]  edge The edge that is opposed to the angle to be
   *  calculated
   *  @return The value of the cotangent of the angle
   */
  double ComputeCotan(const Edge &edge) const;

  /** @brief Compute the angle that is comprised between two
   * edges
   *  @param[in]  a   First edge
   *  @param[in]  b   Second edge
   *  @return The value of the angle
   */
  static double ComputeAngle(const SimpleVector &a, const SimpleVector &b);

  /** @brief Compute the angle that is opposed to an edge
   *  @param[in]  edge The edge that is opposed to the angle to be
   *  calculated
   *  @return The value of the angle
   */
  double ComputeAngle(const Edge &edge) const;

  /** @brief Compute the contribution to the Gauss curvature due to the
   * cell
   *
   *  The returned value is multiplied by the MixedArea such that when
   *  used in the points loop it is not computed twice
   *
   *  @param[in] pc The center point that is the vertex around which to
   *  compute the Gauss curvatur
   *  @return The value of the contribution for the Gauss curvature
   */
  double GA(const Point &pc) const;

  /** @brief Compute the cell quality
   *
   * This method computes the quality associated to the cell. At the
   * moment the method implemented is just the ratio of the radius of
   * the circle inscribed in the triangle and the circle circumscribed
   * to the triangle
   *
   * @todo We should implement a factory pattern or at the least
   * polymorphism to be able to select different quality algorithm.
   *
   */
  double Quality() const;

  // :: Operators ::

  Cell &operator++() {
    m_id++;
    return *this;
  }
  Cell operator++(int) {
    Cell previous(m_mesh, m_id);
    operator++();
    return previous;
  }
  bool operator==(const Cell &other) const { return m_id == other.m_id; }
  bool operator!=(const Cell &other) const { return m_id != other.m_id; }
};
}  // namespace hgve

#endif /* CELL_H */
