/* Mercur(v)e
 * Copyright © 2020 Ruben Di Battista
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPOSITELEVELSETOBJECT_H
#define COMPOSITELEVELSETOBJECT_H

#include <memory>

#include "LevelSetObject.h"

namespace hgve {

/** @brief This is the function that is called on the two single SDF function of
 * the objects */
using CompositeFun = double(double, double);

/** @brief A "noop" function to be used when initializing a
 * CompositeLevelSetObject with a single LevelSetObject for both m_first and
 * m_second
 */
double noop(double first_sdf, double second_sdf);

/**
 * @brief A class representing a composite LevelSetObject coming out of
 * operation between two other LevelSetObjects
 *
 * This class stores pointers to the two LevelSetObject and executes a given
 * function on both of them
 */
class CompositeLevelSetObject : public LevelSetObject {
 private:
  LevelSetObjectPtr m_first;
  LevelSetObjectPtr m_second;
  CompositeFun *m_fun;

 public:
  CompositeLevelSetObject() = default;
  CompositeLevelSetObject(LevelSetObjectPtr first, LevelSetObjectPtr second,
                          CompositeFun *m_fun);
  CompositeLevelSetObject(const CompositeLevelSetObject &);

  double SDF(double x, double y, double z) const override;
  std::unique_ptr<LevelSetObject> clone() const override;

  // :: Operators ::
  CompositeLevelSetObject &operator=(CompositeLevelSetObject &&);
};
}  // namespace hgve

#endif /* COMPOSITELEVELSETOBJECT_H */
