#include <catch2/catch.hpp>

#include "BlockMerger.h"
#include "GridBlock.h"

using namespace hgve;

static void AllocateBlock(vtkImageData *block) {
  // Allocate in the block as value the coordinates of the points
  int *dims = block->GetDimensions();
  for (int i = 0; i < dims[0]; i++) {
    for (int j = 0; j < dims[1]; j++) {
      for (int k = 0; k < dims[2]; k++) {
        block->SetScalarComponentFromDouble(i, j, k, 0, i * j * k);
      }
    }
  }
}

SCENARIO("Merge separate blocks of a simulation into one file", "[merger]") {
  GIVEN("Two blocks, one of them with negative origin coordinates") {
    vtkSmartPointer<vtkImageData> block0 = vtkSmartPointer<vtkImageData>::New();

    // From 0 to 3 in all directions
    block0->SetDimensions(4, 4, 4);
    block0->SetOrigin(0, 0, 0);
    block0->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block0);

    vtkSmartPointer<vtkImageData> block1 = vtkSmartPointer<vtkImageData>::New();

    // From 0 to 3 in x, y. From -4 to -1 in z-direction
    block1->SetDimensions(4, 4, 4);
    block1->SetOrigin(0, 0, -4);
    block1->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block1);

    WHEN("Merging the 2 blocks") {
      BlockMerger merger(std::vector<GridBlock>{
          {block0},
          {block1},
      });
      merger.SetBoundariesOverlapping(false);
      merger.Merge();

      auto mergedBlock = merger.GetOutput();

      THEN("The Merged block has 8 points along the z-coordinater") {
        REQUIRE(mergedBlock->GetDimensions()[2] == 8);
      }

      AND_THEN("The bounds of the block are correctly defined") {
        double *bounds = mergedBlock->GetBounds();
        REQUIRE(bounds[0] == 0);
        REQUIRE(bounds[1] == 3);
        REQUIRE(bounds[2] == 0);
        REQUIRE(bounds[3] == 3);
        REQUIRE(bounds[4] == -4);
        REQUIRE(bounds[5] == 3);
      }
    }
  }

  GIVEN("A vector of 4 equal blocks") {
    // Generate 4 blocks
    vtkSmartPointer<vtkImageData> block0 = vtkSmartPointer<vtkImageData>::New();
    block0->SetDimensions(4, 4, 4);
    block0->SetOrigin(0, 0, 0);
    block0->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block0);

    vtkSmartPointer<vtkImageData> block1 = vtkSmartPointer<vtkImageData>::New();
    block1->SetDimensions(4, 4, 4);
    block1->AllocateScalars(VTK_DOUBLE, 1);
    block1->SetOrigin(0, 0, 4);
    AllocateBlock(block1);

    vtkSmartPointer<vtkImageData> block2 = vtkSmartPointer<vtkImageData>::New();
    block2->SetDimensions(4, 4, 4);
    block2->SetOrigin(0, 4, 0);
    block2->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block2);

    vtkSmartPointer<vtkImageData> block3 = vtkSmartPointer<vtkImageData>::New();
    block3->SetDimensions(4, 4, 4);
    block3->SetOrigin(0, 4, 4);
    block3->AllocateScalars(VTK_DOUBLE, 1);
    AllocateBlock(block3);

    std::vector<GridBlock> blocks = {GridBlock(block0), GridBlock(block3),
                                     GridBlock(block2), GridBlock(block1)};

    WHEN("Merging the 4 blocks") {
      BlockMerger merger(blocks);

      WHEN("Merging with no boundaries overlapping") {
        merger.SetBoundariesOverlapping(false);
        merger.Merge();

        vtkSmartPointer<vtkImageData> mergedBlock = merger.GetOutput();

        THEN(
            "The merged block has 8 points on z and y-axis, and 4 on the "
            "x-axis") {
          int *dims = mergedBlock->GetDimensions();

          REQUIRE(dims[0] == 4);
          REQUIRE(dims[1] == 8);
          REQUIRE(dims[2] == 8);
        }

        AND_THEN("There's some zeros in the point values") {
          // TODO: Couldn't think of a better test
          int *dims = mergedBlock->GetDimensions();
          std::vector<double> pointValues;
          for (int i = 0; i < dims[0]; i++) {
            for (int j = 0; j < dims[1]; j++) {
              for (int k = 0; k < dims[2]; k++) {
                pointValues.push_back(
                    mergedBlock->GetScalarComponentAsDouble(i, j, k, 0));
              }
            }
          }

          REQUIRE(std::any_of(pointValues.cbegin(), pointValues.cend(),
                              [](double p) { return p == 0; }));
        }
      }

      WHEN("Merging with boundaries overlapping") {
        merger.SetBoundariesOverlapping(true);
        merger.Merge();

        vtkSmartPointer<vtkImageData> mergedBlock = merger.GetOutput();

        THEN(
            "The merged block has 8 points on z and y-axis, and 4 on the "
            "x-axis") {
          int *dims = mergedBlock->GetDimensions();

          REQUIRE(dims[0] == 2);
          REQUIRE(dims[1] == 4);
          REQUIRE(dims[2] == 4);
        }

        AND_THEN("There's no zeros in the point values") {
          // TODO: Couldn't think of a better test
          int *dims = mergedBlock->GetDimensions();
          std::vector<double> pointValues;
          for (int i = 0; i < dims[0]; i++) {
            for (int j = 0; j < dims[1]; j++) {
              for (int k = 0; k < dims[2]; k++) {
                pointValues.push_back(
                    mergedBlock->GetScalarComponentAsDouble(i, j, k, 0));
              }
            }
          }

          REQUIRE(std::none_of(pointValues.cbegin(), pointValues.cend(),
                               [](double p) { return p == 0; }));
        }
      }
    }

    WHEN("Assembling the 4 blocks in a vtkMultiBlockDataSet object") {
      vtkSmartPointer<vtkMultiBlockDataSet> multiblock =
          vtkSmartPointer<vtkMultiBlockDataSet>::New();

      multiblock->SetNumberOfBlocks(4);

      multiblock->SetBlock(0, block0);
      multiblock->SetBlock(1, block1);
      multiblock->SetBlock(2, block2);
      multiblock->SetBlock(3, block3);

      WHEN("Merging the blocks with boundaries overlapping") {
        BlockMerger merger(multiblock);
        merger.Merge();
        vtkSmartPointer<vtkImageData> mergedBlock = merger.GetOutput();

        THEN("The merged block has 32 points") {
          REQUIRE(mergedBlock->GetNumberOfPoints() == 32);
        }

        THEN("There's no zeros in the point values") {
          int *dims = mergedBlock->GetDimensions();
          std::vector<double> pointValues;
          for (int i = 0; i < dims[0]; i++) {
            for (int j = 0; j < dims[1]; j++) {
              for (int k = 0; k < dims[2]; k++) {
                pointValues.push_back(
                    mergedBlock->GetScalarComponentAsDouble(i, j, k, 0));
              }
            }
          }

          REQUIRE(std::none_of(pointValues.cbegin(), pointValues.cend(),
                               [](double p) { return p == 0; }));
        }
      }
    }
  }
}
