#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkTriangle.h>

#include <catch2/catch.hpp>

#include "Cell.h"
#include "Iterator.h"
#include "Point.h"
#include "Surface.h"
#include "test_config.h"

const double roofArea = 4 * std::sqrt(2);

using namespace hgve;

SCENARIO("Differential geometry parameters for a roof-shaped patch", "[roof]") {
  GIVEN("A roof-shaped patch") {
    // Points of the roof-shaped patch
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->InsertNextPoint(0, 0, 1);    // #0
    points->InsertNextPoint(0, 1, 1);    // #1
    points->InsertNextPoint(1, 1, 0);    // #2
    points->InsertNextPoint(1, -1, 0);   // #3
    points->InsertNextPoint(0, -1, 1);   // #4
    points->InsertNextPoint(-1, -1, 0);  // #5
    points->InsertNextPoint(-1, 1, 0);   // #6

    // Triangles
    vtkSmartPointer<vtkTriangle> t0 = vtkSmartPointer<vtkTriangle>::New();
    t0->GetPointIds()->SetId(0, 0);
    t0->GetPointIds()->SetId(1, 1);
    t0->GetPointIds()->SetId(2, 2);
    vtkSmartPointer<vtkTriangle> t1 = vtkSmartPointer<vtkTriangle>::New();
    t1->GetPointIds()->SetId(0, 0);
    t1->GetPointIds()->SetId(1, 2);
    t1->GetPointIds()->SetId(2, 3);
    vtkSmartPointer<vtkTriangle> t2 = vtkSmartPointer<vtkTriangle>::New();
    t2->GetPointIds()->SetId(0, 0);
    t2->GetPointIds()->SetId(1, 3);
    t2->GetPointIds()->SetId(2, 4);
    vtkSmartPointer<vtkTriangle> t3 = vtkSmartPointer<vtkTriangle>::New();
    t3->GetPointIds()->SetId(0, 0);
    t3->GetPointIds()->SetId(1, 4);
    t3->GetPointIds()->SetId(2, 5);
    vtkSmartPointer<vtkTriangle> t4 = vtkSmartPointer<vtkTriangle>::New();
    t4->GetPointIds()->SetId(0, 0);
    t4->GetPointIds()->SetId(1, 5);
    t4->GetPointIds()->SetId(2, 6);
    vtkSmartPointer<vtkTriangle> t5 = vtkSmartPointer<vtkTriangle>::New();
    t5->GetPointIds()->SetId(0, 0);
    t5->GetPointIds()->SetId(1, 6);
    t5->GetPointIds()->SetId(2, 1);

    vtkSmartPointer<vtkCellArray> triangles =
        vtkSmartPointer<vtkCellArray>::New();
    triangles->InsertNextCell(t0);
    triangles->InsertNextCell(t1);
    triangles->InsertNextCell(t2);
    triangles->InsertNextCell(t3);
    triangles->InsertNextCell(t4);
    triangles->InsertNextCell(t5);

    vtkSmartPointer<vtkPolyData> patch = vtkSmartPointer<vtkPolyData>::New();
    patch->SetPoints(points);
    patch->SetPolys(triangles);

    // Initialize the surface wrapper
    Surface surface(patch);

    // Handy points
    Point point0(surface, 0);
    Point point1(surface, 1);
    Point point2(surface, 2);
    Point point3(surface, 3);
    Point point4(surface, 4);
    Point point5(surface, 5);
    Point point6(surface, 6);

    REQUIRE(surface.GetNumberOfCells() == 6);
    REQUIRE(surface.GetNumberOfPoints() == 7);

    THEN("The Gauss curvature is zero") {
      REQUIRE(std::abs(point0.OneRingProperties().G()) == Approx(0.0).margin(1e-12));
    }

    THEN("Test surface area is equal to a specific value") {
      REQUIRE(surface.Area() == roofArea);
    }
  }
}
