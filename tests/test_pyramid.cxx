#include <catch2/catch.hpp>

#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkTriangle.h>

#include "Cell.h"
#include "Iterator.h"
#include "Point.h"
#include "Surface.h"

#define TRIANGLE_AREA std::sqrt(3) / 2
#define PYRAMID_AREA 4 * TRIANGLE_AREA

using namespace hgve;

SCENARIO("Differential geometry parameters on a pyramid", "[pyramid]") {
  GIVEN("A pyramid made of 4 points") {
    // Points of the roof-shaped patch
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->InsertNextPoint(0, 0, 1);   // #0
    points->InsertNextPoint(0, 1, 0);   // #1
    points->InsertNextPoint(1, 0, 0);   // #2
    points->InsertNextPoint(0, -1, 0);  // #3
    points->InsertNextPoint(-1, 0, 0);  // #4

    // Triangles
    vtkSmartPointer<vtkTriangle> t0 = vtkSmartPointer<vtkTriangle>::New();
    t0->GetPointIds()->SetId(0, 0);
    t0->GetPointIds()->SetId(1, 1);
    t0->GetPointIds()->SetId(2, 2);
    vtkSmartPointer<vtkTriangle> t1 = vtkSmartPointer<vtkTriangle>::New();
    t1->GetPointIds()->SetId(0, 0);
    t1->GetPointIds()->SetId(1, 2);
    t1->GetPointIds()->SetId(2, 3);
    vtkSmartPointer<vtkTriangle> t2 = vtkSmartPointer<vtkTriangle>::New();
    t2->GetPointIds()->SetId(0, 0);
    t2->GetPointIds()->SetId(1, 3);
    t2->GetPointIds()->SetId(2, 4);
    vtkSmartPointer<vtkTriangle> t3 = vtkSmartPointer<vtkTriangle>::New();
    t3->GetPointIds()->SetId(0, 0);
    t3->GetPointIds()->SetId(1, 4);
    t3->GetPointIds()->SetId(2, 1);

    vtkSmartPointer<vtkCellArray> triangles =
        vtkSmartPointer<vtkCellArray>::New();
    triangles->InsertNextCell(t0);
    triangles->InsertNextCell(t1);
    triangles->InsertNextCell(t2);
    triangles->InsertNextCell(t3);

    vtkSmartPointer<vtkPolyData> patch = vtkSmartPointer<vtkPolyData>::New();
    patch->SetPoints(points);
    patch->SetPolys(triangles);

    // Initialize the surface wrapper
    Surface surface(patch);

    // Handy points
    Point point0(surface, 0);
    Point point1(surface, 1);
    Point point2(surface, 2);
    Point point3(surface, 3);
    Point point4(surface, 4);

    REQUIRE(surface.GetNumberOfCells() == 4);
    REQUIRE(surface.GetNumberOfPoints() == 5);

    Range<Point> pointIterator = surface.IterateOver<Point>();

    WHEN("Checking how many neighbours has each point") {
      auto p0 = pointIterator.begin();

      THEN("The central one has 4 neighbours") {
        REQUIRE(p0->OneRingPoints().size() == 4);
      }

      THEN("All the points out of the central one have 3") {
        for (auto cur_point = ++p0; cur_point != pointIterator.end();
             ++cur_point) {
          REQUIRE(cur_point->OneRingPoints().size() == 3);
        }
      }
    }

    WHEN("Iterating over the 1-ring of the central point") {
      auto p0 = pointIterator.begin();
      THEN("The 1-ring Mixed Area is equal to the Voronoi region") {
        for (auto c : p0->OneRingCells()) {
          REQUIRE(c.MixedArea(*p0) ==
                  Approx(
                      // The roof-shaped patch is composed
                      // by 4 equilateral triangle. The
                      // Voronoi area for an equilateral
                      // triangle is:
                      0.125 * 4 * 1 / std::tan(vtkMath::Pi() / 3)));
        }
      }

      THEN("The 1-ring area is equal to 2*cotan(pi/3)") {
        REQUIRE(p0->OneRingArea() ==
                Approx(2 * 1 / std::tan(vtkMath::Pi() / 3)));
      }

      THEN("The Gauss curvature is equal to 2/3*pi") {
        REQUIRE(p0->OneRingProperties().G() * p0->OneRingArea() ==
                Approx(2.0 / 3.0 * vtkMath::Pi()));
      }

      THEN("The Mean curvature is equal to -1") {
        REQUIRE(p0->OneRingProperties().H() == Approx(-1));
      }
    }

    WHEN("Computing the surface area") {
      for (auto p : surface.IterateOver<Cell>()) {
        THEN("Each point is an equilateral triangle") {
          REQUIRE(p.Area() == TRIANGLE_AREA);
        }
      }
      THEN("The pyramid surface area is correctly computed") {
        REQUIRE(surface.Area() == PYRAMID_AREA);
      }
    }

    WHEN("Computing the angles at each point") {
      double th41 = Cell::ComputeAngle((point4 - point0), (point1 - point0));
      double th12 = Cell::ComputeAngle((point1 - point0), (point2 - point0));
      double th23 = Cell::ComputeAngle((point2 - point0), (point3 - point0));
      double th34 = Cell::ComputeAngle((point3 - point0), (point4 - point0));
      THEN("They are all equal to pi/3") {
        REQUIRE(th41 == Approx(vtkMath::Pi() / 3));
        REQUIRE(th12 == Approx(vtkMath::Pi() / 3));
        REQUIRE(th23 == Approx(vtkMath::Pi() / 3));
        REQUIRE(th34 == Approx(vtkMath::Pi() / 3));
      }
    }
  }
}
