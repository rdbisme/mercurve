#include <catch2/catch.hpp>

#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkTriangle.h>

#include "Surface.h"

using namespace hgve;

TEST_CASE("Test on a single triangle", "[triangle]") {
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkTriangle> t = vtkSmartPointer<vtkTriangle>::New();
  vtkSmartPointer<vtkCellArray> triangles =
      vtkSmartPointer<vtkCellArray>::New();

  vtkSmartPointer<vtkPolyData> patch = vtkSmartPointer<vtkPolyData>::New();

  SECTION("Obtuse Triangle") {
    points->InsertNextPoint(0, 0, 0);
    points->InsertNextPoint(1, 1, 0);
    points->InsertNextPoint(4, 0, 0);

    t->GetPointIds()->SetId(0, 0);
    t->GetPointIds()->SetId(1, 1);
    t->GetPointIds()->SetId(2, 2);

    triangles->InsertNextCell(t);

    patch->SetPoints(points);
    patch->SetPolys(triangles);

    Surface surface(patch);

    REQUIRE(surface.GetNumberOfCells() == 1);
    REQUIRE(surface.GetNumberOfPoints() == 3);

    REQUIRE(surface.Area() == Approx(2.0));

    for (auto c : surface.IterateOver<Cell>()) {
      double A = 0;
      for (auto p : c.Points()) {
        A += c.MixedArea(p);
      }

      REQUIRE(A == Approx(surface.Area()));
      REQUIRE(c.Perimeter() == Approx(std::sqrt(2) + std::sqrt(10) + 4));
    }

    SECTION("Test angles") {
      SimpleVector a(0, 0, 0);
      SimpleVector b(1, 1, 0);
      SimpleVector c(4, 0, 0);

      REQUIRE(Cell::ComputeAngle(b - a, c - a) ==
              Approx(45 * vtkMath::Pi() / 180));
    }
  }

  SECTION("Acute triangle") {
    points->InsertNextPoint(0, 0, 0);
    points->InsertNextPoint(-1, -2, 0);
    points->InsertNextPoint(1, -2, 0);

    t->GetPointIds()->SetId(0, 0);
    t->GetPointIds()->SetId(1, 1);
    t->GetPointIds()->SetId(2, 2);

    triangles->InsertNextCell(t);

    patch->SetPoints(points);
    patch->SetPolys(triangles);

    Surface surface(patch);

    REQUIRE(surface.GetNumberOfCells() == 1);
    REQUIRE(surface.GetNumberOfPoints() == 3);

    REQUIRE(surface.Area() == Approx(2.0));

    for (auto c : surface.IterateOver<Cell>()) {
      double A = 0;
      for (auto p : c.Points()) {
        A += c.MixedArea(p);
      }

      REQUIRE(A == Approx(surface.Area()));
      REQUIRE(c.Perimeter() == Approx(2 * std::sqrt(5) + 2));
    }
  }
}
