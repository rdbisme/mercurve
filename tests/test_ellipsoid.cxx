#include <catch2/catch.hpp>

#include <vtkCleanPolyData.h>
#include <vtkNew.h>
#include <vtkParametricEllipsoid.h>
#include <vtkParametricFunctionSource.h>
#include <vtkSmartPointer.h>
#include <vtkTriangleFilter.h>

#include "Edge.h"
#include "Point.h"
#include "Surface.h"
#include "test_config.h"
#include "types.h"

static const double a = 1.0;  // Semi-axis x
static const double b = 1.5;  // Semi-axis y
static const double c = 2.0;  // Semi-axis z

using namespace hgve;

double ellipsoid_gauss_curvature(double x, double y, double z) {
  double a2 = a * a, b2 = b * b, c2 = c * c, a4 = a2 * a * a, b4 = b2 * b * b,
         c4 = c2 * c * c,
         sqrtDenom = a * b * c * (x * x / a4 + y * y / b4 + z * z / c4);
  return 1 / std::pow(sqrtDenom, 2);
}

double ellipsoid_mean_curvature(double x, double y, double z) {
  double a2 = a * a, b2 = b * b, c2 = c * c, a4 = a2 * a * a, b4 = b2 * b * b,
         c4 = c2 * c * c,
         sqrtDenom = 2 * std::pow(a * b * c, 2) *
                     std::pow((x * x / a4 + y * y / b4 + z * z / c4), 3.0 / 2);

  return std::abs(x * x + y * y + z * z - a2 - b2 - c2) / sqrtDenom;
}

SCENARIO("Differential geometry parameters for an ellipsoid", "[ellipsoid]") {
  GIVEN("An ellipsoid with defined radiuses") {
    vtkSmartPointer<vtkParametricEllipsoid> pEllipse =
        vtkSmartPointer<vtkParametricEllipsoid>::New();
    pEllipse->SetXRadius(a);
    pEllipse->SetYRadius(b);
    pEllipse->SetZRadius(c);

    vtkNew<vtkParametricFunctionSource> ellipsoid;
    ellipsoid->SetParametricFunction(pEllipse);

    WHEN("The ellipsoid has a coarse triangulation") {
      ellipsoid->SetUResolution(coarseResolution);
      ellipsoid->SetVResolution(coarseResolution);
      ellipsoid->SetWResolution(coarseResolution);
      ellipsoid->Update();

      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(ellipsoid->GetOutput());
      triFilter->Update();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());
      surface.Clean(1E-5);
      surface.Sanitize();

      WHEN("Iterating over the 1-ring regions of each point") {
        Set<Point> allPoints;
        double A = 0;
        for (auto p : surface.IterateOver<Point>()) {
          auto oneRingPoints = p.OneRingPoints();
          auto oneRingEdges = p.OneRingEdges();
          A += p.OneRingArea();

          allPoints.insert(oneRingPoints.begin(), oneRingPoints.end());
          THEN("The number of 1-ring edges is equal to the points") {
            REQUIRE(oneRingEdges.size() == oneRingPoints.size());
          }
        }

        AND_THEN(
            "All the 1-ring points are the total amount of "
            "points") {
          REQUIRE(static_cast<int>(allPoints.size()) ==
                  surface.GetNumberOfPoints());
        }

        AND_THEN("The sum of the 1-ring areas is the surface area") {
          REQUIRE(A == Approx(surface.Area()));
        }
      }

      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          AND_THEN("I'm able to save the surface in a file") {
            surface.Write("ellipsoid-coarse.vtp");
          }
        }
      }
    }
    WHEN("The ellipsoid has a fine enough triangulation") {
      ellipsoid->SetUResolution(fineResolution);
      ellipsoid->SetVResolution(fineResolution);
      ellipsoid->SetWResolution(fineResolution);
      ellipsoid->Update();

      vtkNew<vtkTriangleFilter> triFilter;
      triFilter->SetInputData(ellipsoid->GetOutput());
      triFilter->Update();

      // Initialize Surface
      Surface surface(triFilter->GetOutput());
      surface.Clean(1E-5);
      surface.Sanitize();

      THEN(
          "The value of the gauss curvature at each point is equal "
          " to the analytical one") {
        int inliers = 0;
        for (auto p : surface.IterateOver<Point>()) {
          auto coords = p.GetCoordinates();
          double x = coords[0];
          double y = coords[1];
          double z = coords[2];
          if (p.OneRingProperties().G() ==
              Approx(ellipsoid_gauss_curvature(x, y, z)).margin(approxMargin))
            inliers++;
        }

        REQUIRE(float(inliers) / surface.GetNumberOfPoints() > 0.8);
      }

      AND_THEN(
          "The value of the mean curvature at each point is equal "
          " to the analytical one") {
        int inliers = 0;
        for (auto p : surface.IterateOver<Point>()) {
          auto coords = p.GetCoordinates();
          double x = coords[0];
          double y = coords[1];
          double z = coords[2];
          if (p.OneRingProperties().H() ==
              Approx(ellipsoid_mean_curvature(x, y, z)).margin(approxMargin))
            inliers++;
        }

        REQUIRE(float(inliers) / surface.GetNumberOfPoints() > 0.8);
      }

      WHEN("Computing the curvatures from the Surface") {
        THEN("I'm able to store all the points without SIGSEV") {
          surface.ComputeCurvatures();
          AND_THEN("I'm able to save the surface in a file") {
            surface.Write("ellipsoid-fine.vtp");
          }
        }
      }
    }
  }  // GIVEN
}  // SCENARIO
