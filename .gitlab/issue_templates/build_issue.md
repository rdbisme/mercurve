<!-- Please provide information on your operating system
    * Linux
    * macOS
--> 
**My OS is**: 

<!-- Please provide the version of your OS. 
    * Linux: you can provide the output of `cat /etc/os-release`. 
    * macOS: `sw_vers -productVersion` 
-->
**OS Version:**

<!-- Please provide the version of cmake `cmake --version` --> 
**Cmake Version:**

<!-- Please provide the VTK version -->
**VTK version:** 


**`uname -a` output**
<!-- Provide the output of the `uname -a` command -->

```
```

**`env` output**
<!-- Provide the output of `env` command just before starting up the compilation --> 

```
```

**Mercur(v)e origin**
<!-- Describe how you retrieved the Mercur(v)e source code 
    For example `https://gitlab.com/rubendibattista/mercurve.git`
-->

**Mercur(v)e build steps**
<!-- If you compiled Mercur(v)e from source, please provide a detailed step-by-step procedure that you used to build it -->
1. First step
2. Second step

**Mercur(v)e cmake output**
<!-- If you compiled Mercur(v)e from source, please provide cmake command you used and its output --> 
```
```

**Mercur(v)e make output**
<!-- If you compiled Mercur(v)e from source, please provide the output of the `make VERBOSE=1` command -->
```
```


**Steps to reproduce**
<!-- Describe all the steps to reproduce your problem -->

1. First step
2. Second step... 

**Comments**
<!-- Additional comments that can be useful to solve the problem -->
