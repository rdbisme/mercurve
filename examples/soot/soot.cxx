/**
 * This examples creates a soot chain of particles and it uses libMercurve to
 * post-process this chain in terms of geometrical properties.
 */

#include <iterator>
#include <numeric>
#include <sstream>
#include <vector>

#include "CompositeLevelSetObject.h"
#include "LevelSetSphere.h"
#include "Volume.h"

using namespace hgve;

// Geometrical Parameters of the Soot elements
static const double sootRadius = 0.25;
static const double sootDiameter = 2 * sootRadius;
static const double interDistance = 0.9 * sootDiameter;

// Vector containing a list of SootElements
static std::vector<LevelSetSphere> elements = {
    LevelSetSphere{sootRadius, {sootRadius, 0.25, 0.25}},
    LevelSetSphere{sootRadius, {0.25, sootRadius + interDistance, 0.25}},
    LevelSetSphere{sootRadius, {sootRadius + interDistance, 0.25, 0.25}},
    LevelSetSphere{sootRadius, {sootRadius + 2 * interDistance, 0.25, 0.25}}};

// Configuration
static const int resolution = 250;
static const bool average = false;
static const double averagingRadius = 0.1 * sootRadius;

int main(int argc, char *argv[]) {
  Volume domain(resolution, resolution, resolution, SimpleVector(2, 1, 1));

  // Sum first two elements
  auto soot = elements[0] + elements[1];

  // The combine all the other ones
  for (auto el = std::next(elements.begin(), 2); el != elements.end(); ++el) {
    // Combine elements
    soot = soot + *el;
  }

  // Materialize in the domain
  domain.AddLevelSetObject(soot);

  // Save the level-set fields
  domain.Write("soot.vti");

  // Triangulate the domain
  auto surface = domain.Triangulate();

  // Compute curvatures and save
  surface->ComputeCurvatures();
  if (average) {
    surface->Average(averagingRadius);
    surface->Write("soot-average.vtp");
  } else {
    surface->Write("soot.vtp");
  }

  // Test Gauss Bonnet
  double GA = 0;
  for (auto p : surface->IterateOver<Point>()) {
    auto curv = p.OneRingProperties();
    double G = curv.G();
    double A = curv.A();
    GA += G * A;
  }

  std::cout.precision(std::numeric_limits<double>::max_digits10);
  std::cout << "Gauss Bonnet: " << GA / 4 / vtkMath::Pi();
}
