#include "CompositeLevelSetObject.h"
#include "LevelSetSphere.h"
#include "Volume.h"

using namespace hgve;

int main(int argc, char *argv[]) {
  Volume domain(250, 250, 250, SimpleVector(4, 4, 4));

  // Generate the central sphere
  double radius = 1.0;
  SimpleVector center(2, 2, 2);
  LevelSetSphere sphere(radius, center);

  // Generate the ears spheres
  LevelSetSphere ear1(radius / 2, SimpleVector(1, 1, 2));
  LevelSetSphere ear2(radius / 2, SimpleVector(3, 1, 2));

  // Combine objects
  auto mickeyMouse = sphere + ear1 + ear2;

  // Materialize in the domain
  domain.AddLevelSetObject(mickeyMouse);

  // Save the level-set fields
  domain.Write("mickey-mouse.vti");

  // Triangulate the domain
  auto surface = domain.Triangulate();

  // Compute curvatures and save
  surface->ComputeCurvatures();
  surface->Write("mickey-mouse.vtp");

  // Test Gauss Bonnet
  double GA = 0;
  for (auto p : surface->IterateOver<Point>()) {
    auto curv = p.OneRingProperties();
    double G = curv.G();
    double A = curv.A();
    GA += G * A;
  }

  std::cout.precision(std::numeric_limits<double>::max_digits10);
  std::cout << "Gauss Bonnet: " << GA / 4 / vtkMath::Pi();
}
