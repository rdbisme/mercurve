/* ellipsoid-convergence
 * Copyright © 2018 Ruben Di Battista
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing
 * official policies, either expressed or implied, of Ruben Di Battista.
 *
 * This program computes the error on the curvatures computation at different
 * resolutions for an ellipsoid
 */

#include <cmath>
#include <limits>

#include <vtkNew.h>
#include <vtkSuperquadricSource.h>
#include <vtkTriangleFilter.h>

#include "Surface.h"

using namespace hgve;

using dbl = std::numeric_limits<double>;

static const double xAx = 1.0, a = xAx / 2.0;  // Semi-axis x (relative scale)
static const double yAx = 0.5, b = yAx / 2.0;  // Semi-axis y (relative scale)
static const double zAx = 0.5, c = zAx / 2.0;  // Semi-axis z (relative scale)
static const double a2 = a * a, b2 = b * b, c2 = c * c, a4 = a2 * a * a,
                    b4 = b2 * b2, c4 = c2 * c2;

double ellipsoid_gauss_curvature(double x, double y, double z) {
  double sqrtDenom = a * b * c * (x * x / a4 + y * y / b4 + z * z / c4);

  return 1.0 / (std::pow(sqrtDenom, 2));
}

double ellipsoid_mean_curvature(double x, double y, double z) {
  double sqrtDenom =
      2 * std::pow(a * b * c, 2) *
      std::pow((x * x / a4 + y * y / b4 + z * z / c4), 3.0 / 2.0);

  return std::abs(x * x + y * y + z * z - a2 - b2 - c2) / sqrtDenom;
}

double isInvalid(double cur) { return std::isnan(cur) or std::isinf(cur); }

int main(int argc, char *argv[]) {
  int maxNumPoints = 16;
  int numOfRes = 4;

  // Handle arguments. First argument after command is max number of points
  // second number is the number of resolutions steps
  if (argc > 1) {
    maxNumPoints = atoi(argv[1]);
    numOfRes = atoi(argv[2]);
  }

  std::vector<int> resolutions;
  int dR = std::round(maxNumPoints / numOfRes);

  // Allocate different resolutions (each one equal to 4 time the previous)
  int curRes = 0;
  for (int i = 0; i < numOfRes; i++) {
    curRes += dR;
    resolutions.push_back(curRes);
  }

  vtkNew<vtkSuperquadricSource> ellipsoid;
  ellipsoid->SetThetaRoundness(1.0);
  ellipsoid->SetPhiRoundness(1.0);
  ellipsoid->ToroidalOff();
  ellipsoid->SetScale(xAx, yAx, zAx);

  std::cout << "# Number of Points\tError(G)\tError(H)"
            << "\n";

  for (auto res : resolutions) {
    ellipsoid->SetThetaResolution(res);
    ellipsoid->SetPhiResolution(res);
    ellipsoid->Update();

    vtkNew<vtkTriangleFilter> triFilter;
    triFilter->SetInputData(ellipsoid->GetOutput());
    triFilter->PassLinesOff();
    triFilter->PassVertsOff();
    triFilter->Update();

    Surface surface(triFilter->GetOutput());
    surface.Clean(1E-5);
    surface.Sanitize();
    surface.ComputeCurvatures();
    auto curvatures = surface.GetCurvArray();

    auto basename = std::string("ellipsoid-") + std::to_string(res);
    surface.Write(basename + std::string(".vtp"));
    curvatures.Write(basename + std::string(".dat"));

    // Accumulate errors over all the points (L1-norm)
    double errG = 0, errH = 0, errA = 0;
    auto nc = surface.GetNumberOfPoints();

    for (auto p : surface.IterateOver<Point>()) {
      double curG = p.OneRingProperties().G();
      double curH = p.OneRingProperties().H();
      double curA = p.OneRingProperties().A();
      auto coords = p.GetCoordinates();

      double correctG =
          ellipsoid_gauss_curvature(coords[0], coords[1], coords[2]);
      double correctH =
          ellipsoid_mean_curvature(coords[0], coords[1], coords[2]);

      errG += curA * std::abs(curG - correctG);
      errH += curA * std::abs(curH - correctH);
      errA += curA;
    }

    errG /= errA;
    errH /= errA;

    std::cout.precision(dbl::max_digits10);
    std::cout << nc << "\t" << errH << "\t" << errG << std::endl;
  }
}
