# CompilerFlags.cmake
# Copyright © 2018 Ruben Di Battista
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Ruben Di Battista ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Ruben Di Battista BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of Ruben Di Battista.
#
#
# This module configures compiler flags

include(CheckCXXCompilerFlag)

# set_flag_if_supported
#
# This macro checks that the given flag is supported by the compiler and 
# appends it to SUPPORTED_FLAGS
macro(set_flag_if_supported FLAG)
    string(REPLACE "-" "" FLAG_NAME ${FLAG})
    check_cxx_compiler_flag(${FLAG} "${FLAG_NAME}")

    if(${FLAG_NAME})
        list(APPEND SUPPORTED_FLAGS "${FLAG}")
    endif()
endmacro(set_flag_if_supported)


# Handling Debug flags
if(CMAKE_BUILD_TYPE MATCHES Debug)
    list(APPEND REQUIRED_DEBUG_FLAGS 
        "-Wall" 
        "-Wno-long-long" 
        "-pedantic"
        )

    # Force Colored diagnostics for Clang and GCC (useful when using Ninja)
    if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "(Apple)?[Cc]lang")
        list(APPEND REQUIRED_DEBUG_FLAGS
            "-fcolor-diagnostics"
            )
    elseif(CMAKE_COMPILER_IS_GNUCXX)
        list(APPEND REQUIRED_DEBUG_FLAGS
            "-fdiagnostics-color=always"
            )
    endif()

    # Let's add only the compiler flags supported by the compiler
    foreach(flag ${REQUIRED_DEBUG_FLAGS})
        set_flag_if_supported(${flag})
        set(SUPPORTED_ADDITIONAL_FLAGS ${SUPPORTED_FLAGS})
    endforeach()
    
endif(CMAKE_BUILD_TYPE MATCHES Debug)
